layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x380
  RenderBlock {HTML} at (0,0) size 800x380
    RenderBody {BODY} at (8,16) size 784x348
      RenderBlock {P} at (0,0) size 784x20
        RenderText {#text} at (0,0) size 293x19
          text run at (0,0) width 293: "This test is a basic check for using text-overflow."
      RenderBlock {P} at (0,36) size 784x108
        RenderText {#text} at (0,0) size 446x19
          text run at (0,0) width 446: "Apply \"text-overflow:clip\" to inputs. The following input should be clipped:"
        RenderBR {BR} at (446,0) size 0x19
        RenderTextControl {INPUT} at (0,20) size 154x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
        RenderText {#text} at (154,21) size 4x19
          text run at (154,21) width 4: " "
        RenderTextControl {INPUT} at (158,20) size 156x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
          RenderFlexibleBox {DIV} at (3,3) size 150x16
            RenderBlock {DIV} at (0,2.50) size 1x11
            RenderBlock {DIV} at (1,0) size 136x16
        RenderText {#text} at (314,21) size 4x19
          text run at (314,21) width 4: " "
        RenderTextControl {INPUT} at (318,20) size 154x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
        RenderText {#text} at (472,21) size 4x19
          text run at (472,21) width 4: " "
        RenderTextControl {INPUT} at (476,20) size 156x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
          RenderFlexibleBox {DIV} at (3,3) size 150x16
            RenderBlock {DIV} at (0,2.50) size 1x11
            RenderBlock {DIV} at (1,0) size 136x16
        RenderText {#text} at (0,0) size 0x0
        RenderTextControl {INPUT} at (0,42) size 154x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
        RenderBR {BR} at (154,43) size 0x19
        RenderTextControl {INPUT} at (0,64) size 154x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
        RenderText {#text} at (154,65) size 4x19
          text run at (154,65) width 4: " "
        RenderTextControl {INPUT} at (158,64) size 156x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
          RenderFlexibleBox {DIV} at (3,3) size 150x16
            RenderBlock {DIV} at (0,2.50) size 1x11
            RenderBlock {DIV} at (1,0) size 136x16
        RenderText {#text} at (314,65) size 4x19
          text run at (314,65) width 4: " "
        RenderTextControl {INPUT} at (318,64) size 154x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
        RenderText {#text} at (472,65) size 4x19
          text run at (472,65) width 4: " "
        RenderTextControl {INPUT} at (476,64) size 156x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
          RenderFlexibleBox {DIV} at (3,3) size 150x16
            RenderBlock {DIV} at (0,2.50) size 1x11
            RenderBlock {DIV} at (1,0) size 136x16
        RenderText {#text} at (0,0) size 0x0
        RenderTextControl {INPUT} at (0,86) size 154x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
        RenderText {#text} at (0,0) size 0x0
      RenderBlock {P} at (0,160) size 784x108
        RenderText {#text} at (0,0) size 494x19
          text run at (0,0) width 494: "Apply \"text-overflow:ellipsis\" to inputs. The following input should show an ellipsis:"
        RenderBR {BR} at (494,0) size 0x19
        RenderTextControl {INPUT} at (0,20) size 154x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
        RenderText {#text} at (154,21) size 4x19
          text run at (154,21) width 4: " "
        RenderTextControl {INPUT} at (158,20) size 156x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
          RenderFlexibleBox {DIV} at (3,3) size 150x16
            RenderBlock {DIV} at (0,2.50) size 1x11
            RenderBlock {DIV} at (1,0) size 136x16
        RenderText {#text} at (314,21) size 4x19
          text run at (314,21) width 4: " "
        RenderTextControl {INPUT} at (318,20) size 154x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
        RenderText {#text} at (472,21) size 4x19
          text run at (472,21) width 4: " "
        RenderTextControl {INPUT} at (476,20) size 156x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
          RenderFlexibleBox {DIV} at (3,3) size 150x16
            RenderBlock {DIV} at (0,2.50) size 1x11
            RenderBlock {DIV} at (1,0) size 136x16
        RenderText {#text} at (0,0) size 0x0
        RenderTextControl {INPUT} at (0,42) size 154x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
        RenderBR {BR} at (154,43) size 0x19
        RenderTextControl {INPUT} at (0,64) size 154x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
        RenderText {#text} at (154,65) size 4x19
          text run at (154,65) width 4: " "
        RenderTextControl {INPUT} at (158,64) size 156x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
          RenderFlexibleBox {DIV} at (3,3) size 150x16
            RenderBlock {DIV} at (0,2.50) size 1x11
            RenderBlock {DIV} at (1,0) size 136x16
        RenderText {#text} at (314,65) size 4x19
          text run at (314,65) width 4: " "
        RenderTextControl {INPUT} at (318,64) size 154x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
        RenderText {#text} at (472,65) size 4x19
          text run at (472,65) width 4: " "
        RenderTextControl {INPUT} at (476,64) size 156x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
          RenderFlexibleBox {DIV} at (3,3) size 150x16
            RenderBlock {DIV} at (0,2.50) size 1x11
            RenderBlock {DIV} at (1,0) size 136x16
        RenderText {#text} at (0,0) size 0x0
        RenderTextControl {INPUT} at (0,86) size 154x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
        RenderText {#text} at (0,0) size 0x0
      RenderBlock {P} at (0,284) size 784x64
        RenderText {#text} at (0,0) size 218x19
          text run at (0,0) width 218: "Dynamic style change text-overflow:"
        RenderBR {BR} at (218,0) size 0x19
        RenderText {#text} at (0,21) size 223x19
          text run at (0,21) width 223: "Clip to ellipsis (should show ellipsis): "
        RenderTextControl {INPUT} at (223,20) size 154x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
        RenderText {#text} at (377,21) size 4x19
          text run at (377,21) width 4: " "
        RenderTextControl {INPUT} at (381,20) size 154x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
        RenderText {#text} at (535,21) size 4x19
          text run at (535,21) width 4: " "
        RenderBR {BR} at (0,0) size 0x0
        RenderText {#text} at (0,43) size 244x19
          text run at (0,43) width 244: "Ellipsis to clip (should not show ellipsis): "
        RenderTextControl {INPUT} at (244,42) size 154x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
        RenderText {#text} at (398,43) size 4x19
          text run at (398,43) width 4: " "
        RenderTextControl {INPUT} at (402,42) size 154x22 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
        RenderText {#text} at (556,43) size 4x19
          text run at (556,43) width 4: " "
        RenderBR {BR} at (0,0) size 0x0
layer at (10,75) size 150x16 scrollWidth 317
  RenderBlock {DIV} at (2,3) size 150x16 [color=#A9A9A9]
    RenderText {#text} at (0,0) size 317x16
      text run at (0,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (10,75) size 150x16
  RenderBlock {DIV} at (2,3) size 150x16
layer at (170,75) size 136x16 scrollWidth 317
  RenderBlock {DIV} at (4,3) size 136x16 [color=#A9A9A9]
    RenderText {#text} at (0,0) size 317x16
      text run at (0,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (170,75) size 136x16
  RenderBlock {DIV} at (0,0) size 136x16
layer at (328,75) size 150x16 scrollWidth 318
  RenderBlock {DIV} at (2,3) size 150x16
    RenderText {#text} at (0,0) size 317x16
      text run at (0,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (488,75) size 136x16 scrollWidth 318
  RenderBlock {DIV} at (0,0) size 136x16
    RenderText {#text} at (0,0) size 317x16
      text run at (0,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (10,97) size 150x16 scrollWidth 276
  RenderBlock {DIV} at (2,3) size 150x16
    RenderText {#text} at (0,0) size 275x16
      text run at (0,0) width 275: "\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}"
layer at (10,119) size 150x16 scrollX 167.00 scrollWidth 317
  RenderBlock {DIV} at (2,3) size 150x16 [color=#A9A9A9]
    RenderText {#text} at (-167,0) size 317x16
      text run at (-167,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (10,119) size 150x16
  RenderBlock {DIV} at (2,3) size 150x16
layer at (170,119) size 136x16 scrollX 181.00 scrollWidth 317
  RenderBlock {DIV} at (4,3) size 136x16 [color=#A9A9A9]
    RenderText {#text} at (-181,0) size 317x16
      text run at (-181,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (170,119) size 136x16
  RenderBlock {DIV} at (0,0) size 136x16
layer at (328,119) size 150x16 scrollX 167.00 scrollWidth 317
  RenderBlock {DIV} at (2,3) size 150x16
    RenderText {#text} at (-167,0) size 317x16
      text run at (-167,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (488,119) size 136x16 scrollX 181.00 scrollWidth 317
  RenderBlock {DIV} at (0,0) size 136x16
    RenderText {#text} at (-181,0) size 317x16
      text run at (-181,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (10,141) size 150x16 scrollX 125.00 scrollWidth 275
  RenderBlock {DIV} at (2,3) size 150x16
    RenderText {#text} at (-125,0) size 275x16
      text run at (-125,0) width 275 RTL: "\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}"
layer at (10,199) size 150x16 scrollWidth 317
  RenderBlock {DIV} at (2,3) size 150x16 [color=#A9A9A9]
    RenderText {#text} at (0,0) size 317x16
      text run at (0,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (10,199) size 150x16
  RenderBlock {DIV} at (2,3) size 150x16
layer at (170,199) size 136x16 scrollWidth 317
  RenderBlock {DIV} at (4,3) size 136x16 [color=#A9A9A9]
    RenderText {#text} at (0,0) size 317x16
      text run at (0,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (170,199) size 136x16
  RenderBlock {DIV} at (0,0) size 136x16
layer at (328,199) size 150x16 scrollWidth 318
  RenderBlock {DIV} at (2,3) size 150x16
    RenderText {#text} at (0,0) size 317x16
      text run at (0,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (488,199) size 136x16 scrollWidth 318
  RenderBlock {DIV} at (0,0) size 136x16
    RenderText {#text} at (0,0) size 317x16
      text run at (0,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (10,221) size 150x16 scrollWidth 276
  RenderBlock {DIV} at (2,3) size 150x16
    RenderText {#text} at (0,0) size 275x16
      text run at (0,0) width 275: "\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}"
layer at (10,243) size 150x16 scrollX 167.00 scrollWidth 317
  RenderBlock {DIV} at (2,3) size 150x16 [color=#A9A9A9]
    RenderText {#text} at (-167,0) size 317x16
      text run at (-167,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (10,243) size 150x16
  RenderBlock {DIV} at (2,3) size 150x16
layer at (170,243) size 136x16 scrollX 181.00 scrollWidth 317
  RenderBlock {DIV} at (4,3) size 136x16 [color=#A9A9A9]
    RenderText {#text} at (-181,0) size 317x16
      text run at (-181,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (170,243) size 136x16
  RenderBlock {DIV} at (0,0) size 136x16
layer at (328,243) size 150x16 scrollX 167.00 scrollWidth 317
  RenderBlock {DIV} at (2,3) size 150x16
    RenderText {#text} at (-167,0) size 317x16
      text run at (-167,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (488,243) size 136x16 scrollX 181.00 scrollWidth 317
  RenderBlock {DIV} at (0,0) size 136x16
    RenderText {#text} at (-181,0) size 317x16
      text run at (-181,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (10,265) size 150x16 scrollX 125.00 scrollWidth 275
  RenderBlock {DIV} at (2,3) size 150x16
    RenderText {#text} at (-125,0) size 275x16
      text run at (-125,0) width 275 RTL: "\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}"
layer at (233,323) size 150x16 scrollWidth 317
  RenderBlock {DIV} at (2,3) size 150x16 [color=#A9A9A9]
    RenderText {#text} at (0,0) size 317x16
      text run at (0,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (233,323) size 150x16
  RenderBlock {DIV} at (2,3) size 150x16
layer at (391,323) size 150x16 scrollWidth 318
  RenderBlock {DIV} at (2,3) size 150x16
    RenderText {#text} at (0,0) size 317x16
      text run at (0,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (254,345) size 150x16 scrollWidth 317
  RenderBlock {DIV} at (2,3) size 150x16 [color=#A9A9A9]
    RenderText {#text} at (0,0) size 317x16
      text run at (0,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (254,345) size 150x16
  RenderBlock {DIV} at (2,3) size 150x16
layer at (412,345) size 150x16 scrollWidth 318
  RenderBlock {DIV} at (2,3) size 150x16
    RenderText {#text} at (0,0) size 317x16
      text run at (0,0) width 317: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
layer at (307,79) size 9x9 transparent
  RenderBlock {DIV} at (138,3.50) size 9x9
layer at (625,79) size 9x9 transparent
  RenderBlock {DIV} at (138,3.50) size 9x9
layer at (307,123) size 9x9 transparent
  RenderBlock {DIV} at (138,3.50) size 9x9
layer at (625,123) size 9x9 transparent
  RenderBlock {DIV} at (138,3.50) size 9x9
layer at (307,203) size 9x9 transparent
  RenderBlock {DIV} at (138,3.50) size 9x9
layer at (625,203) size 9x9 transparent
  RenderBlock {DIV} at (138,3.50) size 9x9
layer at (307,247) size 9x9 transparent
  RenderBlock {DIV} at (138,3.50) size 9x9
layer at (625,247) size 9x9 transparent
  RenderBlock {DIV} at (138,3.50) size 9x9
