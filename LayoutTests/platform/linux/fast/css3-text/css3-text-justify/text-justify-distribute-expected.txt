layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x93
  RenderBlock {HTML} at (0,0) size 800x93
    RenderBody {BODY} at (8,8) size 784x77
      RenderBlock {DIV} at (0,0) size 784x20
        RenderText {#text} at (0,0) size 595x19
          text run at (0,0) width 595: "This test case is to see whether distributed text justify works properly for the complicated sentence."
      RenderBlock (anonymous) at (0,20) size 784x20
        RenderBR {BR} at (0,0) size 0x19
      RenderBlock {DIV} at (0,40) size 302x17 [border: (1px solid #000000)]
        RenderInline {SPAN} at (0,0) size 29x15
          RenderText {#text} at (1,1) size 29x15
            text run at (1,1) width 29: "L"
        RenderInline {SPAN} at (0,0) size 29x15
          RenderText {#text} at (30,1) size 29x15
            text run at (30,1) width 29: "o"
        RenderInline {SPAN} at (0,0) size 29x15
          RenderText {#text} at (59,1) size 29x15
            text run at (59,1) width 29: "r"
        RenderInline {SPAN} at (0,0) size 29x15
          RenderText {#text} at (88,1) size 29x15
            text run at (88,1) width 29: "e"
        RenderInline {SPAN} at (0,0) size 29x15
          RenderText {#text} at (117,1) size 29x15
            text run at (117,1) width 29: "m"
        RenderText {#text} at (146,1) size 155x15
          text run at (146,1) width 155: " ipsum"
      RenderBlock (anonymous) at (0,57) size 784x20
        RenderBR {BR} at (0,0) size 0x19
