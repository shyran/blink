layer at (0,0) size 480x360
  RenderView at (0,0) size 480x360
layer at (0,0) size 480x360
  RenderSVGRoot {svg} at (0,0) size 480x360
    RenderSVGContainer {g} at (70,9) size 410x266
      RenderSVGText {text} at (114,9) size 252x14 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 251x14
          chunk 1 (middle anchor) text run 1 at (114.93,20.00) startOffset 0 endOffset 50 width 250.14: "Filters: feDistantLight, fePointLight, feSpotLight"
      RenderSVGHiddenContainer {defs} at (0,0) size 0x0
        RenderSVGResourceFilter {filter} [id="distantLightA"] [filterUnits=objectBoundingBox] [primitiveUnits=userSpaceOnUse]
          [feDiffuseLighting surfaceScale="10.00" diffuseConstant="1.00" kernelUnitLength="0.00, 0.00"]
            [SourceGraphic]
        RenderSVGResourceFilter {filter} [id="distantLightB"] [filterUnits=objectBoundingBox] [primitiveUnits=userSpaceOnUse]
          [feDiffuseLighting surfaceScale="10.00" diffuseConstant="1.00" kernelUnitLength="0.00, 0.00"]
            [SourceGraphic]
        RenderSVGResourceFilter {filter} [id="distantLightC"] [filterUnits=objectBoundingBox] [primitiveUnits=userSpaceOnUse]
          [feDiffuseLighting surfaceScale="10.00" diffuseConstant="1.00" kernelUnitLength="0.00, 0.00"]
            [SourceGraphic]
        RenderSVGResourceFilter {filter} [id="distantLightD"] [filterUnits=objectBoundingBox] [primitiveUnits=userSpaceOnUse]
          [feDiffuseLighting surfaceScale="10.00" diffuseConstant="1.00" kernelUnitLength="0.00, 0.00"]
            [SourceGraphic]
        RenderSVGResourceFilter {filter} [id="pointLightA"] [filterUnits=objectBoundingBox] [primitiveUnits=userSpaceOnUse]
          [feDiffuseLighting surfaceScale="10.00" diffuseConstant="1.00" kernelUnitLength="0.00, 0.00"]
            [SourceGraphic]
        RenderSVGResourceFilter {filter} [id="pointLightB"] [filterUnits=objectBoundingBox] [primitiveUnits=userSpaceOnUse]
          [feDiffuseLighting surfaceScale="10.00" diffuseConstant="1.00" kernelUnitLength="0.00, 0.00"]
            [SourceGraphic]
        RenderSVGResourceFilter {filter} [id="pointLightC"] [filterUnits=objectBoundingBox] [primitiveUnits=userSpaceOnUse]
          [feDiffuseLighting surfaceScale="10.00" diffuseConstant="1.00" kernelUnitLength="0.00, 0.00"]
            [SourceGraphic]
        RenderSVGResourceFilter {filter} [id="pointLightD"] [filterUnits=objectBoundingBox] [primitiveUnits=userSpaceOnUse]
          [feDiffuseLighting surfaceScale="10.00" diffuseConstant="1.00" kernelUnitLength="0.00, 0.00"]
            [SourceGraphic]
        RenderSVGResourceFilter {filter} [id="spotLightA"] [filterUnits=objectBoundingBox] [primitiveUnits=userSpaceOnUse]
          [feDiffuseLighting surfaceScale="10.00" diffuseConstant="1.00" kernelUnitLength="0.00, 0.00"]
            [SourceGraphic]
        RenderSVGResourceFilter {filter} [id="spotLightB"] [filterUnits=objectBoundingBox] [primitiveUnits=userSpaceOnUse]
          [feDiffuseLighting surfaceScale="10.00" diffuseConstant="1.00" kernelUnitLength="0.00, 0.00"]
            [SourceGraphic]
        RenderSVGResourceFilter {filter} [id="spotLightC"] [filterUnits=objectBoundingBox] [primitiveUnits=userSpaceOnUse]
          [feDiffuseLighting surfaceScale="10.00" diffuseConstant="1.00" kernelUnitLength="0.00, 0.00"]
            [SourceGraphic]
        RenderSVGResourceFilter {filter} [id="spotLightD"] [filterUnits=objectBoundingBox] [primitiveUnits=userSpaceOnUse]
          [feDiffuseLighting surfaceScale="10.00" diffuseConstant="1.00" kernelUnitLength="0.00, 0.00"]
            [SourceGraphic]
      RenderSVGContainer {g} at (70,38) size 361x62 [transform={m=((1.00,0.00)(0.00,1.00)) t=(0.00,40.00)}]
        RenderSVGText {text} at (70,-2) size 361x16 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 361x16
            chunk 1 text run 1 at (70.00,10.00) startOffset 0 endOffset 55 width 360.78: "Various values for feDistantLight azimuth and elevation"
        RenderSVGText {text} at (70,13) size 35x16 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 35x16
            chunk 1 text run 1 at (70.00,25.00) startOffset 0 endOffset 6 width 34.66: "(0, 0)"
        RenderSVGText {text} at (165,13) size 43x16 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 43x16
            chunk 1 text run 1 at (165.00,25.00) startOffset 0 endOffset 7 width 42.66: "(45, 0)"
        RenderSVGText {text} at (260,13) size 43x16 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 43x16
            chunk 1 text run 1 at (260.00,25.00) startOffset 0 endOffset 7 width 42.66: "(0, 45)"
        RenderSVGText {text} at (355,13) size 51x16 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 51x16
            chunk 1 text run 1 at (355.00,25.00) startOffset 0 endOffset 8 width 50.66: "(45, 45)"
        RenderSVGImage {image} at (70,70) size 50x30
          [filter="distantLightA"] RenderSVGResourceFilter {filter} at (70,30) size 50x30
        RenderSVGImage {image} at (165,70) size 50x30
          [filter="distantLightB"] RenderSVGResourceFilter {filter} at (165,30) size 50x30
        RenderSVGImage {image} at (260,70) size 50x30
          [filter="distantLightC"] RenderSVGResourceFilter {filter} at (260,30) size 50x30
        RenderSVGImage {image} at (355,70) size 50x30
          [filter="distantLightD"] RenderSVGResourceFilter {filter} at (355,30) size 50x30
      RenderSVGContainer {g} at (70,118) size 360x31 [transform={m=((1.00,0.00)(0.00,1.00)) t=(0.00,120.00)}]
        RenderSVGText {text} at (70,-2) size 279x16 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 279x16
            chunk 1 text run 1 at (70.00,10.00) startOffset 0 endOffset 44 width 278.38: "Various values for fePointLight's x, y and z"
        RenderSVGText {text} at (70,13) size 59x16 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 59x16
            chunk 1 text run 1 at (70.00,25.00) startOffset 0 endOffset 10 width 58.66: "(0, 0, 10)"
        RenderSVGText {text} at (165,13) size 67x16 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 67x16
            chunk 1 text run 1 at (165.00,25.00) startOffset 0 endOffset 11 width 66.66: "(50, 0, 10)"
        RenderSVGText {text} at (260,13) size 67x16 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 67x16
            chunk 1 text run 1 at (260.00,25.00) startOffset 0 endOffset 11 width 66.66: "(0, 30, 10)"
        RenderSVGText {text} at (355,13) size 75x16 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 75x16
            chunk 1 text run 1 at (355.00,25.00) startOffset 0 endOffset 12 width 74.66: "(50, 30, 10)"
      RenderSVGContainer {g} at (70,150) size 335x30
        RenderSVGImage {image} at (70,150) size 50x30 [transform={m=((1.00,0.00)(0.00,1.00)) t=(70.00,150.00)}]
          [filter="pointLightA"] RenderSVGResourceFilter {filter} at (0,0) size 50x30
        RenderSVGImage {image} at (165,150) size 50x30 [transform={m=((1.00,0.00)(0.00,1.00)) t=(165.00,150.00)}]
          [filter="pointLightB"] RenderSVGResourceFilter {filter} at (0,0) size 50x30
        RenderSVGImage {image} at (260,150) size 50x30 [transform={m=((1.00,0.00)(0.00,1.00)) t=(260.00,150.00)}]
          [filter="pointLightC"] RenderSVGResourceFilter {filter} at (0,0) size 50x30
        RenderSVGImage {image} at (355,150) size 50x30 [transform={m=((1.00,0.00)(0.00,1.00)) t=(355.00,150.00)}]
          [filter="pointLightD"] RenderSVGResourceFilter {filter} at (0,0) size 50x30
      RenderSVGContainer {g} at (70,198) size 410x46 [transform={m=((1.00,0.00)(0.00,1.00)) t=(0.00,210.00)}]
        RenderSVGText {text} at (70,-12) size 474x16 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 474x16
            chunk 1 text run 1 at (70.00,0.00) startOffset 0 endOffset 73 width 473.07: "Various values for feSpotLight's x, y, z, pointsAtX, pointsAtY, pointsAtZ"
        RenderSVGText {text} at (70,3) size 67x16 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 67x16
            chunk 1 text run 1 at (70.00,15.00) startOffset 0 endOffset 11 width 66.66: "(25, 0, 25)"
        RenderSVGText {text} at (70,18) size 67x16 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 67x16
            chunk 1 text run 1 at (70.00,30.00) startOffset 0 endOffset 11 width 66.66: "(25, 30, 0)"
        RenderSVGText {text} at (165,3) size 75x16 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 75x16
            chunk 1 text run 1 at (165.00,15.00) startOffset 0 endOffset 12 width 74.66: "(25, 30, 25)"
        RenderSVGText {text} at (165,18) size 59x16 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 59x16
            chunk 1 text run 1 at (165.00,30.00) startOffset 0 endOffset 10 width 58.66: "(25, 0, 0)"
        RenderSVGText {text} at (260,18) size 34x16 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 34x16
            chunk 1 text run 1 at (260.00,30.00) startOffset 0 endOffset 4 width 33.02: "np=1"
        RenderSVGText {text} at (355,18) size 149x16 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 149x16
            chunk 1 text run 1 at (355.00,30.00) startOffset 0 endOffset 20 width 148.57: "limitingConeAngle=30"
      RenderSVGContainer {g} at (70,245) size 335x30
        RenderSVGRect {rect} at (70,245) size 50x30 [transform={m=((1.00,0.00)(0.00,1.00)) t=(70.00,245.00)}] [fill={[type=SOLID] [color=#000000]}] [x=0.00] [y=0.00] [width=50.00] [height=30.00]
          [filter="spotLightA"] RenderSVGResourceFilter {filter} at (0,0) size 50x30
        RenderSVGRect {rect} at (165,245) size 50x30 [transform={m=((1.00,0.00)(0.00,1.00)) t=(165.00,245.00)}] [fill={[type=SOLID] [color=#000000]}] [x=0.00] [y=0.00] [width=50.00] [height=30.00]
          [filter="spotLightB"] RenderSVGResourceFilter {filter} at (0,0) size 50x30
        RenderSVGRect {rect} at (260,245) size 50x30 [transform={m=((1.00,0.00)(0.00,1.00)) t=(260.00,245.00)}] [fill={[type=SOLID] [color=#000000]}] [x=0.00] [y=0.00] [width=50.00] [height=30.00]
          [filter="spotLightC"] RenderSVGResourceFilter {filter} at (0,0) size 50x30
        RenderSVGRect {rect} at (355,245) size 50x30 [transform={m=((1.00,0.00)(0.00,1.00)) t=(355.00,245.00)}] [fill={[type=SOLID] [color=#000000]}] [x=0.00] [y=0.00] [width=50.00] [height=30.00]
          [filter="spotLightD"] RenderSVGResourceFilter {filter} at (0,0) size 50x30
    RenderSVGText {text} at (10,310) size 264x40 contains 1 chunk(s)
      RenderSVGInlineText {#text} at (0,0) size 264x40
        chunk 1 text run 1 at (10.00,340.00) startOffset 0 endOffset 16 width 263.34: "$Revision: 1.8 $"
    RenderSVGRect {rect} at (0,0) size 480x360 [stroke={[type=SOLID] [color=#000000]}] [x=1.00] [y=1.00] [width=478.00] [height=358.00]
