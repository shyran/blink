layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock (anonymous) at (0,0) size 784x16
        RenderText {#text} at (0,0) size 707x16
          text run at (0,0) width 707: "This test demonstrates that list items are not affected by the quirk that shrinks line boxes with no text children."
      RenderBlock {UL} at (0,32) size 784x60
        RenderListItem {LI} at (40,0) size 744x30
          RenderListMarker at (-17,9) size 10x10
          RenderInline {SPAN} at (0,0) size 27x16
            RenderText {#text} at (0,7) size 27x16
              text run at (0,7) width 27: "One"
          RenderText {#text} at (0,0) size 0x0
        RenderListItem {LI} at (40,30) size 744x30
          RenderListMarker at (-17,9) size 10x10
          RenderInline {SPAN} at (0,0) size 30x16
            RenderText {#text} at (0,7) size 30x16
              text run at (0,7) width 30: "Two"
          RenderText {#text} at (0,0) size 0x0
      RenderBlock {UL} at (0,108) size 784x60
        RenderListItem {LI} at (40,0) size 744x30
          RenderInline {SPAN} at (0,0) size 38x16
            RenderText {#text} at (0,7) size 38x16
              text run at (0,7) width 38: "Three"
          RenderText {#text} at (0,0) size 0x0
        RenderListItem {LI} at (40,30) size 744x30
          RenderInline {SPAN} at (0,0) size 31x16
            RenderText {#text} at (0,7) size 31x16
              text run at (0,7) width 31: "Four"
          RenderText {#text} at (0,0) size 0x0
