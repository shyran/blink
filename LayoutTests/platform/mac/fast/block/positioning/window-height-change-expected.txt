layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x32
        RenderText {#text} at (0,0) size 218x16
          text run at (0,0) width 218: "This tests for a regression against "
        RenderInline {I} at (0,0) size 771x32
          RenderInline {A} at (0,0) size 355x16 [color=#0000EE]
            RenderText {#text} at (217,0) size 355x16
              text run at (217,0) width 355: "http://bugzilla.opendarwin.org/show_bug.cgi?id=5813"
          RenderText {#text} at (571,0) size 771x32
            text run at (571,0) width 5: " "
            text run at (575,0) width 196: "positioned blocks don't update"
            text run at (0,16) width 231: "when resizing the window vertically"
        RenderText {#text} at (230,16) size 5x16
          text run at (230,16) width 5: "."
      RenderBlock {P} at (0,48) size 784x16
        RenderText {#text} at (0,0) size 293x16
          text run at (0,0) width 293: "The two squares below should be solid green."
      RenderBlock {HR} at (0,80) size 784x2 [border: (1px inset #EEEEEE)]
      RenderBlock (anonymous) at (0,90) size 784x108
        RenderText {#text} at (104,92) size 4x16
          text run at (104,92) width 4: " "
        RenderText {#text} at (0,0) size 0x0
layer at (8,98) size 104x104
  RenderPartObject {IFRAME} at (0,0) size 104x104 [border: (2px inset #EEEEEE)]
    layer at (0,0) size 100x100
      RenderView at (0,0) size 100x100
    layer at (0,0) size 100x8
      RenderBlock {HTML} at (0,0) size 100x8
        RenderBody {BODY} at (8,8) size 84x0 [bgcolor=#FF0000]
    layer at (0,0) size 100x100
      RenderBlock (positioned) {DIV} at (0,0) size 100x100 [bgcolor=#008000]
layer at (116,98) size 104x104
  RenderPartObject {IFRAME} at (108,0) size 104x104 [border: (2px inset #EEEEEE)]
    layer at (0,0) size 100x100
      RenderView at (0,0) size 100x100
    layer at (0,0) size 100x16
      RenderDeprecatedFlexibleBox {HTML} at (0,0) size 100x16
        RenderBody {BODY} at (8,8) size 0x0 [bgcolor=#FF0000]
    layer at (0,0) size 100x100
      RenderBlock (positioned) {DIV} at (0,0) size 100x100 [bgcolor=#008000]
