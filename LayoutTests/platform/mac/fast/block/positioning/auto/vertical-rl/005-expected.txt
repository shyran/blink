layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderText {#text} at (0,0) size 80x574
        text run at (0,0) width 571: "In this test, you should see three blocks that are aligned to the bottom within a black box."
        text run at (16,0) width 543: "They should be stacked horizontally with the green box in between two olive boxes. "
        text run at (16,542) width 25: "The"
        text run at (32,0) width 326: "olive boxes and the green box should be 100x100. "
        text run at (32,325) width 222: "The black box's left side should be"
        text run at (48,0) width 574: "aligned with the last line in this paragraph and its top side should begin right after the end"
        text run at (64,0) width 104: "of this sentence."
      RenderText {#text} at (0,0) size 0x0
layer at (388,112) size 340x240
  RenderBlock (positioned) {SPAN} at (72,111.53) size 340x240 [border: (20px solid #000000)]
layer at (508,232) size 100x100
  RenderBlock (positioned) {DIV} at (120,120) size 100x100 [bgcolor=#008000]
layer at (608,232) size 100x100
  RenderBlock (positioned) {DIV} at (20,120) size 100x100 [bgcolor=#808000]
layer at (408,232) size 100x100
  RenderBlock (positioned) {DIV} at (220,120) size 100x100 [bgcolor=#808000]
