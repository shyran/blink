layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {H3} at (0,0) size 784x19
        RenderText {#text} at (0,0) size 578x19
          text run at (0,0) width 578: "Test case for First-line text-decoration style inherited from Parent Block"
      RenderBlock {P} at (0,37.72) size 784x16
        RenderText {#text} at (0,0) size 462x16
          text run at (0,0) width 462: "The First line text and its text-decoration must be of the same color."
      RenderBlock {DIV} at (0,69.72) size 784x96 [color=#FF0000]
        RenderBlock {P} at (0,0) size 784x64 [color=#008000]
          RenderText {#text} at (0,12) size 626x24
            text run at (0,12) width 626: "Underline Underline Underline Underline"
          RenderBR {BR} at (625,30) size 1x0
          RenderText {#text} at (0,48) size 286x16
            text run at (0,48) width 286: "Underline Underline Underline Underline"
          RenderBR {BR} at (285,60) size 1x0
        RenderBlock (anonymous) at (0,80) size 784x16
          RenderText {#text} at (0,0) size 268x16
            text run at (0,0) width 268: "Underline Underline Underline Underline"
      RenderBlock (anonymous) at (0,165.72) size 784x16
        RenderBR {BR} at (0,0) size 0x16
      RenderBlock {DIV} at (0,197.72) size 784x96 [color=#FF0000]
        RenderBlock {P} at (0,0) size 784x64 [color=#008000]
          RenderText {#text} at (0,12) size 553x24
            text run at (0,12) width 553: "Overline Overline Overline Overline"
          RenderBR {BR} at (552,30) size 1x0
          RenderText {#text} at (0,48) size 251x16
            text run at (0,48) width 251: "Overline Overline Overline Overline"
          RenderBR {BR} at (250,60) size 1x0
        RenderBlock (anonymous) at (0,80) size 784x16
          RenderText {#text} at (0,0) size 236x16
            text run at (0,0) width 236: "Overline Overline Overline Overline"
      RenderBlock (anonymous) at (0,293.72) size 784x16
        RenderBR {BR} at (0,0) size 0x16
      RenderBlock {DIV} at (0,325.72) size 784x96 [color=#FF0000]
        RenderBlock {P} at (0,0) size 784x64 [color=#008000]
          RenderText {#text} at (0,12) size 721x24
            text run at (0,12) width 721: "Line-Through Line-Through Line-Through Line "
          RenderBR {BR} at (720,30) size 1x0
          RenderText {#text} at (0,48) size 400x16
            text run at (0,48) width 400: "Line-Through Line-Through Line-Through Line-Through"
          RenderBR {BR} at (399,60) size 1x0
        RenderBlock (anonymous) at (0,80) size 784x16
          RenderText {#text} at (0,0) size 371x16
            text run at (0,0) width 371: "Line-Through Line-Through Line-Through Line-Through"
      RenderBlock (anonymous) at (0,421.72) size 784x16
        RenderBR {BR} at (0,0) size 0x16
