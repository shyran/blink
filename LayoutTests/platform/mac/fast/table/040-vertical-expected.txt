layer at (0,0) size 800x600 clip at (0,0) size 785x600 scrollHeight 2321
  RenderView at (0,0) size 800x600
layer at (0,0) size 785x2321 backgroundClip at (0,0) size 785x600 clip at (0,0) size 785x600 outlineClip at (0,0) size 785x600
  RenderBlock {HTML} at (0,0) size 785x2321.44
    RenderBody {BODY} at (8,8) size 769x2305.44
      RenderBlock {H1} at (0,0) size 769x32
        RenderText {#text} at (0,0) size 613x32
          text run at (0,0) width 613: "Fixed Columns, Auto Span, Minheight Table"
      RenderTable {TABLE} at (0,53.44) size 40x100
        RenderTableSection {TBODY} at (0,0) size 40x100
          RenderTableRow {TR} at (0,0) size 20x100
            RenderTableCell {TD} at (0,10) size 20x13 [bgcolor=#00FFFF] [r=0 c=0 rs=1 cs=1]
            RenderTableCell {TD} at (0,43) size 20x47 [bgcolor=#FFFF00] [r=0 c=1 rs=1 cs=1]
          RenderTableRow {TR} at (0,20) size 20x100
            RenderTableCell {TD} at (20,10) size 20x80 [bgcolor=#FFC0CB] [r=1 c=0 rs=1 cs=2]
              RenderBlock {DIV} at (10,-10) size 0x100
      RenderBlock {P} at (0,169.44) size 769x48
        RenderText {#text} at (0,0) size 152x16
          text run at (0,0) width 152: "The table height is: 100"
        RenderBR {BR} at (151,12) size 1x0
        RenderText {#text} at (0,16) size 135x16
          text run at (0,16) width 135: "Column One is: 33%"
        RenderBR {BR} at (134,28) size 1x0
        RenderText {#text} at (0,32) size 138x16
          text run at (0,32) width 138: "Column Two is: 67%"
      RenderBlock {HR} at (0,233.44) size 769x2 [border: (1px inset #EEEEEE)]
      RenderTable {TABLE} at (0,243.44) size 40x600
        RenderTableSection {TBODY} at (0,0) size 40x600
          RenderTableRow {TR} at (0,0) size 20x600
            RenderTableCell {TD} at (0,10) size 20x180 [bgcolor=#00FFFF] [r=0 c=0 rs=1 cs=1]
            RenderTableCell {TD} at (0,210) size 20x380 [bgcolor=#FFFF00] [r=0 c=1 rs=1 cs=1]
          RenderTableRow {TR} at (0,20) size 20x600
            RenderTableCell {TD} at (20,10) size 20x580 [bgcolor=#FFC0CB] [r=1 c=0 rs=1 cs=2]
              RenderBlock {DIV} at (10,-10) size 0x600
      RenderBlock {P} at (0,859.44) size 769x48
        RenderText {#text} at (0,0) size 152x16
          text run at (0,0) width 152: "The table height is: 600"
        RenderBR {BR} at (151,12) size 1x0
        RenderText {#text} at (0,16) size 135x16
          text run at (0,16) width 135: "Column One is: 33%"
        RenderBR {BR} at (134,28) size 1x0
        RenderText {#text} at (0,32) size 138x16
          text run at (0,32) width 138: "Column Two is: 67%"
      RenderBlock {HR} at (0,923.44) size 769x2 [border: (1px inset #EEEEEE)]
      RenderTable {TABLE} at (0,933.44) size 52x600
        RenderTableSection {TBODY} at (0,0) size 52x600
          RenderTableRow {TR} at (0,0) size 32x600
            RenderTableCell {TD} at (0,0) size 32x200 [bgcolor=#00FFFF] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (0,0) size 32x192
                text run at (0,0) width 192: "Fixed cell in column one with"
                text run at (16,0) width 66: "some text."
            RenderTableCell {TD} at (0,200) size 32x400 [bgcolor=#FFFF00] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (0,0) size 32x389
                text run at (0,0) width 389: "Fixed cell in column two with a lot more text. Will the ratios"
                text run at (16,0) width 89: "be preserved?"
          RenderTableRow {TR} at (0,32) size 20x600
            RenderTableCell {TD} at (32,10) size 20x580 [bgcolor=#FFC0CB] [r=1 c=0 rs=1 cs=2]
              RenderBlock {DIV} at (10,-10) size 0x600
      RenderBlock {P} at (0,1549.44) size 769x48
        RenderText {#text} at (0,0) size 152x16
          text run at (0,0) width 152: "The table height is: 600"
        RenderBR {BR} at (151,12) size 1x0
        RenderText {#text} at (0,16) size 135x16
          text run at (0,16) width 135: "Column One is: 33%"
        RenderBR {BR} at (134,28) size 1x0
        RenderText {#text} at (0,32) size 138x16
          text run at (0,32) width 138: "Column Two is: 67%"
      RenderBlock {HR} at (0,1613.44) size 769x2 [border: (1px inset #EEEEEE)]
      RenderTable {TABLE} at (0,1623.44) size 40x600
        RenderTableSection {TBODY} at (0,0) size 40x600
          RenderTableRow {TR} at (0,0) size 20x600
            RenderTableCell {TD} at (0,10) size 20x180 [bgcolor=#00FFFF] [r=0 c=0 rs=1 cs=1]
              RenderBlock {DIV} at (10,-10) size 0x100
            RenderTableCell {TD} at (0,210) size 20x380 [bgcolor=#FFFF00] [r=0 c=1 rs=1 cs=1]
              RenderBlock {DIV} at (10,-10) size 0x250
          RenderTableRow {TR} at (0,20) size 20x600
            RenderTableCell {TD} at (20,10) size 20x580 [bgcolor=#FFC0CB] [r=1 c=0 rs=1 cs=2]
              RenderBlock {DIV} at (10,-10) size 0x600
      RenderBlock {P} at (0,2239.44) size 769x48
        RenderText {#text} at (0,0) size 152x16
          text run at (0,0) width 152: "The table height is: 600"
        RenderBR {BR} at (151,12) size 1x0
        RenderText {#text} at (0,16) size 135x16
          text run at (0,16) width 135: "Column One is: 33%"
        RenderBR {BR} at (134,28) size 1x0
        RenderText {#text} at (0,32) size 138x16
          text run at (0,32) width 138: "Column Two is: 67%"
      RenderBlock {HR} at (0,2303.44) size 769x2 [border: (1px inset #EEEEEE)]
