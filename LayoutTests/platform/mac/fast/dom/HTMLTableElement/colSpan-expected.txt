layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x16
        RenderText {#text} at (0,0) size 172x16
          text run at (0,0) width 172: "Tests: the colspan attribute"
        RenderBR {BR} at (171,12) size 1x0
      RenderBlock {P} at (0,32) size 784x64
        RenderText {#text} at (0,0) size 75x16
          text run at (0,0) width 75: "Conditions:"
        RenderBR {BR} at (74,12) size 1x0
        RenderText {#text} at (0,16) size 763x48
          text run at (0,16) width 668: "The colSpan attribute specifies the number of columns spanned by the current cell. The default value of "
          text run at (667,16) width 96: "this attribute is"
          text run at (0,32) width 551: "one (\"1\"). The value zero (\"0\") means that the cell spans all columns from the current "
          text run at (550,32) width 208: "column to the last column of the"
          text run at (0,48) width 372: "column group (COLGROUP) in which the cell is defined."
      RenderBlock {P} at (0,112) size 784x48
        RenderText {#text} at (0,0) size 776x48
          text run at (0,0) width 776: "The first table tests the default value of one, and an input of 7, which is within the normal range of expect input. The first"
          text run at (0,16) width 722: "column in the first row should span only column \"one\" and the second cell should span all of the other columns. "
          text run at (721,16) width 26: "The"
          text run at (0,32) width 572: "second table tests the zero value. \"Just A\" should span \"A\" and \"Just B\" should span \"B.\""
      RenderBlock {HR} at (0,176) size 784x2 [border: (1px inset #EEEEEE)]
      RenderBlock (anonymous) at (0,186) size 784x16
        RenderBR {BR} at (0,0) size 0x16
      RenderTable {TABLE} at (0,202) size 278x64 [border: (1px outset #808080)]
        RenderBlock {CAPTION} at (0,0) size 278x16
          RenderText {#text} at (29,0) size 220x16
            text run at (29,0) width 220: "Testing Default and Regular Input"
        RenderTableSection {TBODY} at (1,17) size 276x46
          RenderTableRow {TR} at (0,2) size 276x20
            RenderTableCell {TD} at (2,2) size 34x20 [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 30x16
                text run at (2,2) width 30: "First"
            RenderTableCell {TD} at (38,2) size 236x20 [border: (1px inset #808080)] [r=0 c=1 rs=1 cs=7]
              RenderText {#text} at (2,2) size 77x16
                text run at (2,2) width 77: "All the Rest"
          RenderTableRow {TR} at (0,24) size 276x20
            RenderTableCell {TD} at (2,24) size 34x20 [border: (1px inset #808080)] [r=1 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 24x16
                text run at (2,2) width 24: "one"
            RenderTableCell {TD} at (38,24) size 28x20 [border: (1px inset #808080)] [r=1 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 24x16
                text run at (2,2) width 24: "two"
            RenderTableCell {TD} at (68,24) size 36x20 [border: (1px inset #808080)] [r=1 c=2 rs=1 cs=1]
              RenderText {#text} at (2,2) size 32x16
                text run at (2,2) width 32: "three"
            RenderTableCell {TD} at (106,24) size 31x20 [border: (1px inset #808080)] [r=1 c=3 rs=1 cs=1]
              RenderText {#text} at (2,2) size 27x16
                text run at (2,2) width 27: "four"
            RenderTableCell {TD} at (139,24) size 29x20 [border: (1px inset #808080)] [r=1 c=4 rs=1 cs=1]
              RenderText {#text} at (2,2) size 25x16
                text run at (2,2) width 25: "five"
            RenderTableCell {TD} at (170,24) size 23x20 [border: (1px inset #808080)] [r=1 c=5 rs=1 cs=1]
              RenderText {#text} at (2,2) size 19x16
                text run at (2,2) width 19: "six"
            RenderTableCell {TD} at (195,24) size 41x20 [border: (1px inset #808080)] [r=1 c=6 rs=1 cs=1]
              RenderText {#text} at (2,2) size 37x16
                text run at (2,2) width 37: "seven"
            RenderTableCell {TD} at (238,24) size 36x20 [border: (1px inset #808080)] [r=1 c=7 rs=1 cs=1]
              RenderText {#text} at (2,2) size 32x16
                text run at (2,2) width 32: "eight"
      RenderBlock (anonymous) at (0,266) size 784x16
        RenderBR {BR} at (0,0) size 0x16
      RenderTable {TABLE} at (0,282) size 199x64 [border: (1px outset #808080)]
        RenderBlock {CAPTION} at (0,0) size 199x16
          RenderText {#text} at (15,0) size 169x16
            text run at (15,0) width 169: "Testing Zero Special Case"
        RenderTableSection {TBODY} at (1,17) size 197x46
          RenderTableRow {TR} at (0,2) size 197x20
            RenderTableCell {TD} at (2,2) size 45x20 [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 41x16
                text run at (2,2) width 41: "Just A"
            RenderTableCell {TD} at (49,2) size 44x20 [border: (1px inset #808080)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 40x16
                text run at (2,2) width 40: "Just B"
          RenderTableRow {TR} at (0,24) size 197x20
            RenderTableCell {TD} at (2,24) size 45x20 [border: (1px inset #808080)] [r=1 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 12x16
                text run at (2,2) width 12: "A"
            RenderTableCell {TD} at (49,24) size 44x20 [border: (1px inset #808080)] [r=1 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 11x16
                text run at (2,2) width 11: "B"
            RenderTableCell {TD} at (95,24) size 15x20 [border: (1px inset #808080)] [r=1 c=2 rs=1 cs=1]
              RenderText {#text} at (2,2) size 11x16
                text run at (2,2) width 11: "C"
            RenderTableCell {TD} at (112,24) size 16x20 [border: (1px inset #808080)] [r=1 c=3 rs=1 cs=1]
              RenderText {#text} at (2,2) size 12x16
                text run at (2,2) width 12: "D"
            RenderTableCell {TD} at (130,24) size 14x20 [border: (1px inset #808080)] [r=1 c=4 rs=1 cs=1]
              RenderText {#text} at (2,2) size 10x16
                text run at (2,2) width 10: "E"
            RenderTableCell {TD} at (146,24) size 13x20 [border: (1px inset #808080)] [r=1 c=5 rs=1 cs=1]
              RenderText {#text} at (2,2) size 9x16
                text run at (2,2) width 9: "F"
            RenderTableCell {TD} at (161,24) size 16x20 [border: (1px inset #808080)] [r=1 c=6 rs=1 cs=1]
              RenderText {#text} at (2,2) size 12x16
                text run at (2,2) width 12: "G"
            RenderTableCell {TD} at (179,24) size 16x20 [border: (1px inset #808080)] [r=1 c=7 rs=1 cs=1]
              RenderText {#text} at (2,2) size 12x16
                text run at (2,2) width 12: "H"
