layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x480
  RenderBlock {HTML} at (0,0) size 800x480
    RenderBody {BODY} at (8,8) size 784x456
      RenderBlock (anonymous) at (0,0) size 784x424
        RenderText {#text} at (302,90) size 4x16
          text run at (302,90) width 4: " "
        RenderText {#text} at (0,0) size 0x0
        RenderText {#text} at (302,196) size 4x16
          text run at (302,196) width 4: " "
        RenderText {#text} at (0,0) size 0x0
        RenderText {#text} at (302,302) size 4x16
          text run at (302,302) width 4: " "
        RenderText {#text} at (0,0) size 0x0
        RenderText {#text} at (302,408) size 4x16
          text run at (302,408) width 4: " "
        RenderText {#text} at (0,0) size 0x0
      RenderBlock {P} at (0,440) size 784x16
        RenderText {#text} at (0,0) size 366x16
          text run at (0,0) width 366: "Tests that image alt text takes directionality into account."
layer at (8,8) size 302x102 clip at (9,9) size 300x100 scrollHeight 134
  RenderBlock (relative positioned) {SECTION} at (0,0) size 302x102 [border: (1px solid #FF0000)]
    RenderBlock {H2} at (1,20.91) size 300x24
      RenderText {#text} at (0,0) size 163x24
        text run at (0,0) width 163: "ltr image, rtl alt"
layer at (9,59) size 280x80 backgroundClip at (9,9) size 300x100 clip at (9,9) size 300x100 outlineClip at (9,9) size 300x100
  RenderBlock (positioned) {IMG} at (1,51) size 280x80
layer at (9,59) size 280x80 backgroundClip at (9,59) size 280x50 clip at (10,60) size 278x49 outlineClip at (9,9) size 300x100
  RenderBlock {DIV} at (0,0) size 280x80 [border: (1px solid #C0C0C0)]
    RenderImage {IMG} at (2,2) size 16x16
layer at (27,61) size 260x21
  RenderBlock {DIV} at (18,2) size 260x21
    RenderText {#text} at (0,3) size 10x16
      text run at (0,3) width 10 RTL: "\x{6CC}\x{627}"
layer at (314,8) size 302x102 clip at (315,9) size 300x100 scrollHeight 134
  RenderBlock (relative positioned) {SECTION} at (306,0) size 302x102 [border: (1px solid #FF0000)]
    RenderBlock {H2} at (1,20.91) size 300x24
      RenderText {#text} at (0,0) size 163x24
        text run at (0,0) width 163: "rtl image, rtl alt"
layer at (315,59) size 280x80 backgroundClip at (315,9) size 300x100 clip at (315,9) size 300x100 outlineClip at (315,9) size 300x100
  RenderBlock (positioned) {IMG} at (1,51) size 280x80
layer at (315,59) size 280x80 backgroundClip at (315,59) size 280x50 clip at (316,60) size 278x49 outlineClip at (315,9) size 300x100
  RenderBlock {DIV} at (0,0) size 280x80 [border: (1px solid #C0C0C0)]
    RenderImage {IMG} at (262,2) size 16x16
layer at (317,61) size 260x21
  RenderBlock {DIV} at (2,2) size 260x21
    RenderText {#text} at (250,3) size 10x16
      text run at (250,3) width 10 RTL: "\x{6CC}\x{627}"
layer at (8,114) size 302x102 clip at (9,115) size 300x100 scrollHeight 134
  RenderBlock (relative positioned) {SECTION} at (0,106) size 302x102 [border: (1px solid #FF0000)]
    RenderBlock {H2} at (1,20.91) size 300x24
      RenderText {#text} at (0,0) size 163x24
        text run at (0,0) width 163: "ltr image, ltr alt"
layer at (9,165) size 280x80 backgroundClip at (9,115) size 300x100 clip at (9,115) size 300x100 outlineClip at (9,115) size 300x100
  RenderBlock (positioned) {IMG} at (1,51) size 280x80
layer at (9,165) size 280x80 backgroundClip at (9,165) size 280x50 clip at (10,166) size 278x49 outlineClip at (9,115) size 300x100
  RenderBlock {DIV} at (0,0) size 280x80 [border: (1px solid #C0C0C0)]
    RenderImage {IMG} at (2,2) size 16x16
layer at (27,167) size 260x16
  RenderBlock {DIV} at (18,2) size 260x16
    RenderText {#text} at (0,0) size 16x16
      text run at (0,0) width 16: "alt"
layer at (314,114) size 302x102 clip at (315,115) size 300x100 scrollHeight 134
  RenderBlock (relative positioned) {SECTION} at (306,106) size 302x102 [border: (1px solid #FF0000)]
    RenderBlock {H2} at (1,20.91) size 300x24
      RenderText {#text} at (0,0) size 163x24
        text run at (0,0) width 163: "rtl image, ltr alt"
layer at (315,165) size 280x80 backgroundClip at (315,115) size 300x100 clip at (315,115) size 300x100 outlineClip at (315,115) size 300x100
  RenderBlock (positioned) {IMG} at (1,51) size 280x80
layer at (315,165) size 280x80 backgroundClip at (315,165) size 280x50 clip at (316,166) size 278x49 outlineClip at (315,115) size 300x100
  RenderBlock {DIV} at (0,0) size 280x80 [border: (1px solid #C0C0C0)]
    RenderImage {IMG} at (262,2) size 16x16
layer at (317,167) size 260x16
  RenderBlock {DIV} at (2,2) size 260x16
    RenderText {#text} at (244,0) size 16x16
      text run at (244,0) width 16: "alt"
layer at (8,220) size 302x102 clip at (9,221) size 300x100 scrollHeight 134
  RenderBlock (relative positioned) {SECTION} at (0,212) size 302x102 [border: (1px solid #FF0000)]
    RenderBlock {H2} at (1,20.91) size 300x24
      RenderText {#text} at (0,0) size 191x24
        text run at (0,0) width 191: "ltr image, weak alt"
layer at (9,271) size 280x80 backgroundClip at (9,221) size 300x100 clip at (9,221) size 300x100 outlineClip at (9,221) size 300x100
  RenderBlock (positioned) {IMG} at (1,51) size 280x80
layer at (9,271) size 280x80 backgroundClip at (9,271) size 280x50 clip at (10,272) size 278x49 outlineClip at (9,221) size 300x100
  RenderBlock {DIV} at (0,0) size 280x80 [border: (1px solid #C0C0C0)]
    RenderImage {IMG} at (2,2) size 16x16
layer at (27,273) size 260x16
  RenderBlock {DIV} at (18,2) size 260x16
    RenderText {#text} at (0,0) size 8x16
      text run at (0,0) width 8: ".,"
layer at (314,220) size 302x102 clip at (315,221) size 300x100 scrollHeight 134
  RenderBlock (relative positioned) {SECTION} at (306,212) size 302x102 [border: (1px solid #FF0000)]
    RenderBlock {H2} at (1,20.91) size 300x24
      RenderText {#text} at (0,0) size 191x24
        text run at (0,0) width 191: "rtl image, weak alt"
layer at (315,271) size 280x80 backgroundClip at (315,221) size 300x100 clip at (315,221) size 300x100 outlineClip at (315,221) size 300x100
  RenderBlock (positioned) {IMG} at (1,51) size 280x80
layer at (315,271) size 280x80 backgroundClip at (315,271) size 280x50 clip at (316,272) size 278x49 outlineClip at (315,221) size 300x100
  RenderBlock {DIV} at (0,0) size 280x80 [border: (1px solid #C0C0C0)]
    RenderImage {IMG} at (262,2) size 16x16
layer at (317,273) size 260x16
  RenderBlock {DIV} at (2,2) size 260x16
    RenderText {#text} at (252,0) size 8x16
      text run at (252,0) width 8 RTL: ".,"
layer at (8,326) size 302x102 clip at (9,327) size 300x100 scrollHeight 134
  RenderBlock (relative positioned) {SECTION} at (0,318) size 302x102 [border: (1px solid #FF0000)]
    RenderBlock {H2} at (1,20.91) size 300x24
      RenderText {#text} at (0,0) size 200x24
        text run at (0,0) width 200: "ltr image, mixed alt"
layer at (9,377) size 280x80 backgroundClip at (9,327) size 300x100 clip at (9,327) size 300x100 outlineClip at (9,327) size 300x100
  RenderBlock (positioned) {IMG} at (1,51) size 280x80
layer at (9,377) size 280x80 backgroundClip at (9,377) size 280x50 clip at (10,378) size 278x49 outlineClip at (9,327) size 300x100
  RenderBlock {DIV} at (0,0) size 280x80 [border: (1px solid #C0C0C0)]
    RenderImage {IMG} at (2,2) size 16x16
layer at (27,379) size 260x21
  RenderBlock {DIV} at (18,2) size 260x21
    RenderText {#text} at (0,3) size 74x16
      text run at (0,3) width 10 RTL: "\x{6CC}\x{627}"
      text run at (9,3) width 55: "example"
      text run at (63,3) width 11 RTL: "\x{6CC}\x{627}"
layer at (314,326) size 302x102 clip at (315,327) size 300x100 scrollHeight 134
  RenderBlock (relative positioned) {SECTION} at (306,318) size 302x102 [border: (1px solid #FF0000)]
    RenderBlock {H2} at (1,20.91) size 300x24
      RenderText {#text} at (0,0) size 200x24
        text run at (0,0) width 200: "rtl image, mixed alt"
layer at (315,377) size 280x80 backgroundClip at (315,327) size 300x100 clip at (315,327) size 300x100 outlineClip at (315,327) size 300x100
  RenderBlock (positioned) {IMG} at (1,51) size 280x80
layer at (315,377) size 280x80 backgroundClip at (315,377) size 280x50 clip at (316,378) size 278x49 outlineClip at (315,327) size 300x100
  RenderBlock {DIV} at (0,0) size 280x80 [border: (1px solid #C0C0C0)]
    RenderImage {IMG} at (262,2) size 16x16
layer at (317,379) size 260x21
  RenderBlock {DIV} at (2,2) size 260x21
    RenderText {#text} at (186,3) size 74x16
      text run at (186,3) width 11 RTL: "\x{6CC}\x{627}"
      text run at (196,3) width 55: "example"
      text run at (250,3) width 10 RTL: "\x{6CC}\x{627}"
