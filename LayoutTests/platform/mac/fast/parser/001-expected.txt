layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {DIR} at (0,0) size 784x96
        RenderBlock (anonymous) at (40,0) size 744x16
          RenderText {#text} at (0,0) size 27x16
            text run at (0,0) width 27: "One"
        RenderBlock {DIR} at (40,32) size 744x64
          RenderBlock (anonymous) at (40,0) size 704x16
            RenderText {#text} at (0,0) size 30x16
              text run at (0,0) width 30: "Two"
          RenderBlock {DIR} at (40,32) size 704x32
            RenderBlock (anonymous) at (40,0) size 664x16
              RenderText {#text} at (0,0) size 38x16
                text run at (0,0) width 38: "Three"
            RenderBlock {CENTER} at (40,16) size 664x16
              RenderText {#text} at (316,0) size 32x16
                text run at (316,0) width 32: "Four"
      RenderBlock {PRE} at (0,112) size 784x329
        RenderText {#text} at (0,0) size 63x13
          text run at (0,0) width 63: "Pre text"
          text run at (62,0) width 1: " "
        RenderText {#text} at (300,303) size 300x26
          text run at (300,303) width 0: " "
          text run at (0,316) width 102: "Also pre text"
          text run at (101,316) width 1: " "
      RenderBlock (anonymous) at (0,454) size 784x16
        RenderInline {NOBR} at (0,0) size 139x16 [bgcolor=#CCCCCC]
          RenderText {#text} at (0,0) size 139x16
            text run at (0,0) width 139: "This text won't break."
      RenderBlock (anonymous) at (0,483) size 784x13
        RenderBlock {PRE} at (0,0) size 784x13
          RenderText {#text} at (0,0) size 203x13
            text run at (0,0) width 203: "This will be preformatted."
      RenderBlock (anonymous) at (0,509) size 784x16
        RenderInline {NOBR} at (0,0) size 236x16 [bgcolor=#CCCCCC]
          RenderText {#text} at (0,0) size 236x16
            text run at (0,0) width 236: "Now we're back to nonbreaking text."
        RenderText {#text} at (0,0) size 0x0
layer at (8,133) size 300x300
  RenderEmbeddedObject {OBJECT} at (0,13) size 300x300 [bgcolor=#008000]
