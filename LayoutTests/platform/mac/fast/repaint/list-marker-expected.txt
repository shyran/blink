layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock {P} at (0,0) size 784x32
        RenderText {#text} at (0,0) size 55x16
          text run at (0,0) width 55: "Test for "
        RenderInline {I} at (0,0) size 741x32
          RenderInline {A} at (0,0) size 306x16 [color=#0000EE]
            RenderText {#text} at (54,0) size 306x16
              text run at (54,0) width 306: "http://bugs.webkit.org/show_bug.cgi?id=12910"
          RenderText {#text} at (359,0) size 741x32
            text run at (359,0) width 5: " "
            text run at (363,0) width 378: "REGRESSION (r18756-r18765): list-bullet doesn't redraw"
            text run at (0,16) width 376: "properly when changing the list's content using JavaScript"
        RenderText {#text} at (375,16) size 5x16
          text run at (375,16) width 5: "."
      RenderBlock {UL} at (0,48) size 784x16
        RenderListItem {LI} at (40,0) size 744x16
          RenderBlock (anonymous) at (0,0) size 744x16
            RenderListMarker at (-16,0) size 6x16: bullet
            RenderText {#text} at (0,0) size 22x16
              text run at (0,0) width 22: "foo"
          RenderBlock {DIV} at (10,26) size 724x0
      RenderBlock {UL} at (0,80) size 784x16
        RenderListItem {LI} at (40,0) size 744x16
          RenderBlock (anonymous) at (0,0) size 744x16
            RenderListMarker at (-1,0) size 6x16: bullet
            RenderText {#text} at (12,0) size 21x16
              text run at (12,0) width 21: "bar"
          RenderBlock {DIV} at (10,26) size 724x0
      RenderBlock {UL} at (0,112) size 784x16
        RenderListItem {LI} at (0,0) size 744x16
          RenderBlock (anonymous) at (0,0) size 744x16
            RenderListMarker at (754,0) size 6x16: bullet
            RenderText {#text} at (722,0) size 22x16
              text run at (722,0) width 22: "foo"
          RenderBlock {DIV} at (10,26) size 724x0
      RenderBlock {UL} at (0,144) size 784x16
        RenderListItem {LI} at (0,0) size 744x16
          RenderBlock (anonymous) at (0,0) size 744x16
            RenderListMarker at (739,0) size 6x16: bullet
            RenderText {#text} at (711,0) size 21x16
              text run at (711,0) width 21: "bar"
          RenderBlock {DIV} at (10,26) size 724x0
