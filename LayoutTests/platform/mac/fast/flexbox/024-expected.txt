layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x48
        RenderText {#text} at (0,0) size 764x48
          text run at (0,0) width 764: "You should see a single 100x100 green square below. If your browser supports the display of ALT text, you should see"
          text run at (0,16) width 273: "the word \"Image\" inside the green square. "
          text run at (272,16) width 275: "If you see any red, then the test has failed. "
          text run at (546,16) width 218: "This test is checking to make sure"
          text run at (0,32) width 327: "replaced elements inside boxes get stretched when "
        RenderInline {CODE} at (0,0) size 141x13
          RenderText {#text} at (326,34) size 141x13
            text run at (326,34) width 141: "box-align: stretch"
        RenderText {#text} at (466,32) size 82x16
          text run at (466,32) width 82: " is specified."
      RenderDeprecatedFlexibleBox {DIV} at (0,64) size 100x100 [bgcolor=#FF0000]
        RenderBlock {IMG} at (0,0) size 100x100 [bgcolor=#008000]
layer at (8,72) size 100x100 clip at (9,73) size 98x98
  RenderBlock {DIV} at (0,0) size 100x100 [border: (1px solid #C0C0C0)]
layer at (10,74) size 96x16
  RenderBlock {DIV} at (2,2) size 96x16
    RenderText {#text} at (0,0) size 40x16
      text run at (0,0) width 40: "Image"
