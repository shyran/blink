layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderText {#text} at (0,0) size 566x16
        text run at (0,0) width 256: "This tests that styled popups look right. "
        text run at (255,0) width 311: "(Aqua for now- later, we will honor the styling)."
      RenderBR {BR} at (565,12) size 1x0
      RenderMenuList {SELECT} at (0,16) size 51x18 [bgcolor=#FF0000] [border: (1px solid #A6A6A6)]
        RenderBlock (anonymous) at (1,1) size 49x16
          RenderText at (8,1) size 20x13
            text run at (8,1) width 20: "test"
      RenderText {#text} at (51,17) size 4x16
        text run at (51,17) width 4: " "
      RenderBR {BR} at (55,29) size 0x0
      RenderText {#text} at (0,34) size 541x16
        text run at (0,34) width 541: "This tests that background color is white by default regardless of the parent element."
      RenderBR {BR} at (540,46) size 1x0
      RenderInline {SPAN} at (0,0) size 65x26 [bgcolor=#FF0000]
        RenderText {#text} at (0,0) size 0x0
        RenderMenuList {SELECT} at (5,50) size 51x18 [bgcolor=#F8F8F8]
          RenderBlock (anonymous) at (0,0) size 51x18
            RenderText at (8,2) size 20x13
              text run at (8,2) width 20: "test"
        RenderText {#text} at (56,51) size 4x16
          text run at (56,51) width 4: " "
      RenderText {#text} at (0,0) size 0x0
      RenderBR {BR} at (0,0) size 0x0
      RenderText {#text} at (0,68) size 642x16
        text run at (0,68) width 642: "This tests that background color is inherited from the parent if background-color:inherit is specified."
      RenderBR {BR} at (641,80) size 1x0
      RenderInline {SPAN} at (0,0) size 65x26 [bgcolor=#FF0000]
        RenderText {#text} at (0,0) size 0x0
        RenderMenuList {SELECT} at (5,84) size 51x18 [border: (1px solid #A6A6A6)]
          RenderBlock (anonymous) at (1,1) size 49x16
            RenderText at (8,1) size 20x13
              text run at (8,1) width 20: "test"
        RenderText {#text} at (56,85) size 4x16
          text run at (56,85) width 4: " "
      RenderText {#text} at (0,0) size 0x0
      RenderBR {BR} at (0,0) size 0x0
      RenderText {#text} at (0,102) size 653x16
        text run at (0,102) width 653: "This tests that background color is the same as the parent if background-color:transparent is specified."
      RenderBR {BR} at (652,114) size 1x0
      RenderInline {SPAN} at (0,0) size 65x26 [bgcolor=#FF0000]
        RenderText {#text} at (0,0) size 0x0
        RenderMenuList {SELECT} at (5,118) size 51x18 [border: (1px solid #A6A6A6)]
          RenderBlock (anonymous) at (1,1) size 49x16
            RenderText at (8,1) size 20x13
              text run at (8,1) width 20: "test"
        RenderText {#text} at (56,119) size 4x16
          text run at (56,119) width 4: " "
      RenderText {#text} at (0,0) size 0x0
      RenderBR {BR} at (0,0) size 0x0
      RenderText {#text} at (0,136) size 509x16
        text run at (0,136) width 509: "This tests that background is white if only background-image:none is specified."
      RenderBR {BR} at (508,148) size 1x0
      RenderMenuList {SELECT} at (0,152) size 51x18 [bgcolor=#F8F8F8]
        RenderBlock (anonymous) at (0,0) size 51x18
          RenderText at (8,2) size 20x13
            text run at (8,2) width 20: "test"
      RenderText {#text} at (51,153) size 4x16
        text run at (51,153) width 4: " "
      RenderBR {BR} at (55,165) size 0x0
      RenderText {#text} at (0,170) size 430x16
        text run at (0,170) width 430: "This tests that the image specified for background-image is visible."
      RenderBR {BR} at (429,182) size 1x0
      RenderMenuList {SELECT} at (0,186) size 51x18 [bgcolor=#F8F8F8] [border: (1px solid #A6A6A6)]
        RenderBlock (anonymous) at (1,1) size 49x16
          RenderText at (8,1) size 20x13
            text run at (8,1) width 20: "test"
      RenderText {#text} at (0,0) size 0x0
