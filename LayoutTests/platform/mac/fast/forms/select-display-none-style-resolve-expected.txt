layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock {P} at (0,0) size 784x32
        RenderText {#text} at (0,0) size 55x16
          text run at (0,0) width 55: "Test for "
        RenderInline {I} at (0,0) size 781x32
          RenderInline {A} at (0,0) size 306x16 [color=#0000EE]
            RenderText {#text} at (54,0) size 306x16
              text run at (54,0) width 306: "http://bugs.webkit.org/show_bug.cgi?id=13896"
          RenderText {#text} at (359,0) size 781x32
            text run at (359,0) width 5: " "
            text run at (363,0) width 418: "REGRESSION (NativePopup): Reproductible crasher on Google"
            text run at (0,16) width 125: "Coop control panel"
        RenderText {#text} at (124,16) size 5x16
          text run at (124,16) width 5: "."
      RenderBlock {P} at (0,48) size 784x16
        RenderText {#text} at (0,0) size 180x16
          text run at (0,0) width 180: "No crash means SUCCESS."
