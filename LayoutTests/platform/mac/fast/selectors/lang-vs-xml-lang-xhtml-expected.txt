layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x80
  RenderBlock {html} at (0,0) size 800x80
    RenderBlock {div} at (0,16) size 800x48
      RenderBlock {p} at (0,0) size 800x16 [bgcolor=#008000]
        RenderText {#text} at (0,0) size 272x16
          text run at (0,0) width 272: "This line should have a green background."
      RenderBlock {p} at (0,32) size 800x16 [bgcolor=#0000FF]
        RenderText {#text} at (0,0) size 309x16
          text run at (0,0) width 309: "This line should NOT have a green background."
