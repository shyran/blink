layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock {P} at (0,0) size 784x16
        RenderText {#text} at (0,0) size 55x16
          text run at (0,0) width 55: "Test for "
        RenderInline {A} at (0,0) size 69x16 [color=#0000EE]
          RenderText {#text} at (54,0) size 69x16
            text run at (54,0) width 69: "bug 11141"
        RenderText {#text} at (122,0) size 346x16
          text run at (122,0) width 9: ": "
          text run at (130,0) width 338: "CSS '@import' doesn't respect HTML Base element."
      RenderBlock {P} at (0,32) size 784x16 [color=#008000]
        RenderText {#text} at (0,0) size 166x16
          text run at (0,0) width 166: "This text should be green."
      RenderBlock {P} at (0,64) size 784x48
        RenderText {#text} at (0,0) size 780x48
          text run at (0,0) width 387: "If it is red, the css has been loaded relative to the document. "
          text run at (386,0) width 394: "If it is black, no stylesheet has been rendered, if it is rendered"
          text run at (0,16) width 44: "green, "
          text run at (43,16) width 474: "the stylesheet has been rendered correctly from the HREF attribute of the "
          text run at (516,16) width 242: "Base element in the HEAD section of"
          text run at (0,32) width 95: "this document."
      RenderBlock {P} at (0,128) size 784x16 [color=#008000]
        RenderText {#text} at (0,0) size 196x16
          text run at (0,0) width 196: "This text should also be green."
