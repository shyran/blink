layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x240
  RenderBlock {HTML} at (0,0) size 800x240
    RenderBody {BODY} at (8,16) size 784x120
      RenderBlock {P} at (0,0) size 784x40 [color=#000080]
        RenderText {#text} at (0,6) size 408x16
          text run at (0,6) width 252: "In the following test, the purple square "
          text run at (251,6) width 157: "should be on the left (\x{21E6}"
        RenderImage {IMG} at (407.45,3) size 19x19.19
        RenderText {#text} at (426,6) size 221x16
          text run at (426,6) width 221: "), and the teal square on the right ("
        RenderImage {IMG} at (646.38,3) size 19x19.19
        RenderText {#text} at (665,6) size 760x34
          text run at (665,6) width 95: "\x{21E8}) of the blue"
          text run at (0,24) width 63: "rectangle."
      RenderBlock {DIV} at (0,56) size 784x64
        RenderBlock {DIV} at (16,0) size 752x64 [color=#0000FF] [bgcolor=#000080]
          RenderImage {IMG} at (8,8) size 160x160
          RenderImage {IMG} at (584,8) size 160x160
          RenderText {#text} at (168,8) size 400x48
            text run at (168,8) width 400: "Blue rectangle. Blue rectangle. Blue rectangle. Blue rectangle."
            text run at (168,24) width 202: "Blue rectangle. Blue rectangle. "
            text run at (369,24) width 199: "Blue rectangle. Blue rectangle."
            text run at (168,40) width 400: "Blue rectangle. Blue rectangle. Blue rectangle. Blue rectangle."
