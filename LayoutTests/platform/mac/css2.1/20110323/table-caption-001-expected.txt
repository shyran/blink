layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x168
  RenderBlock {HTML} at (0,0) size 800x168
    RenderBody {BODY} at (8,16) size 784x144
      RenderBlock {P} at (0,0) size 784x16
        RenderText {#text} at (0,0) size 308x16
          text run at (0,0) width 308: "Test passes if the \"Filler Text\" is above the box."
      RenderTable {DIV} at (0,32) size 96x112
        RenderBlock {DIV} at (0,0) size 96x16
          RenderText {#text} at (0,0) size 68x16
            text run at (0,0) width 68: "Filler Text"
        RenderTableSection (anonymous) at (0,16) size 96x96
          RenderTableRow {DIV} at (0,0) size 96x48
            RenderTableCell {DIV} at (0,0) size 48x0 [bgcolor=#000000] [r=0 c=0 rs=1 cs=1]
            RenderTableCell {DIV} at (48,0) size 48x0 [bgcolor=#000000] [r=0 c=1 rs=1 cs=1]
          RenderTableRow {DIV} at (0,48) size 96x48
            RenderTableCell {DIV} at (0,48) size 48x0 [bgcolor=#000000] [r=1 c=0 rs=1 cs=1]
            RenderTableCell {DIV} at (48,48) size 48x0 [bgcolor=#000000] [r=1 c=1 rs=1 cs=1]
