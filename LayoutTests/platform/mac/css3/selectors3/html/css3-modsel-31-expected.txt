layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x352
  RenderBlock {HTML} at (0,0) size 800x352
    RenderBody {BODY} at (8,16) size 784x320
      RenderBlock {P} at (0,0) size 784x16 [bgcolor=#00FF00]
        RenderText {#text} at (0,0) size 297x16
          text run at (0,0) width 297: "This paragraph should have green background"
      RenderBlock {ADDRESS} at (0,32) size 784x16
        RenderText {#text} at (0,0) size 340x16
          text run at (0,0) width 340: "But this address is here only to fill space in the dom.."
      RenderBlock {P} at (0,64) size 784x16
        RenderText {#text} at (0,0) size 155x16
          text run at (0,0) width 155: "So does this paragraph !"
      RenderBlock {P} at (0,96) size 784x16
        RenderText {#text} at (0,0) size 162x16
          text run at (0,0) width 162: "And so does this one too."
      RenderBlock {DL} at (0,128) size 784x192
        RenderBlock {DT} at (0,0) size 784x16
          RenderText {#text} at (0,0) size 129x16
            text run at (0,0) width 129: "First definition term"
        RenderBlock {DD} at (40,16) size 744x16
          RenderText {#text} at (0,0) size 96x16
            text run at (0,0) width 96: "First definition"
        RenderBlock {DT} at (0,32) size 784x16
          RenderText {#text} at (0,0) size 147x16
            text run at (0,0) width 147: "Second definition term"
        RenderBlock {DD} at (40,48) size 744x16
          RenderText {#text} at (0,0) size 114x16
            text run at (0,0) width 114: "Second definition"
        RenderBlock {DT} at (0,64) size 784x16 [bgcolor=#00FF00]
          RenderText {#text} at (0,0) size 364x16
            text run at (0,0) width 364: "Third definition term that should have green background"
        RenderBlock {DD} at (40,80) size 744x16 [bgcolor=#00FF00]
          RenderText {#text} at (0,0) size 330x16
            text run at (0,0) width 330: "Third definition that should have green background"
        RenderBlock {DT} at (0,96) size 784x16
          RenderText {#text} at (0,0) size 143x16
            text run at (0,0) width 143: "Fourth definition term"
        RenderBlock {DD} at (40,112) size 744x16
          RenderText {#text} at (0,0) size 109x16
            text run at (0,0) width 109: "Fourth definition"
        RenderBlock {DT} at (0,128) size 784x16
          RenderText {#text} at (0,0) size 131x16
            text run at (0,0) width 131: "Fifth definition term"
        RenderBlock {DD} at (40,144) size 744x16
          RenderText {#text} at (0,0) size 98x16
            text run at (0,0) width 98: "Fifth definition"
        RenderBlock {DT} at (0,160) size 784x16 [bgcolor=#00FF00]
          RenderText {#text} at (0,0) size 362x16
            text run at (0,0) width 362: "Sixth definition term that should have green background"
        RenderBlock {DD} at (40,176) size 744x16 [bgcolor=#00FF00]
          RenderText {#text} at (0,0) size 328x16
            text run at (0,0) width 328: "Sixth definition that should have green background"
