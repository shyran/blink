layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x506
  RenderBlock {HTML} at (0,0) size 800x506
    RenderBody {BODY} at (8,8) size 784x482 [bgcolor=#999999]
      RenderTable {TABLE} at (0,0) size 152x58 [border: (1px outset #808080)]
        RenderBlock {CAPTION} at (0,0) size 152x32
          RenderText {#text} at (11,0) size 130x32
            text run at (11,0) width 130: "Table with no width"
            text run at (49,16) width 54: "attribute"
        RenderTableCol {COLGROUP} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
        RenderTableSection {TBODY} at (1,33) size 150x24
          RenderTableRow {TR} at (0,2) size 150x20
            RenderTableCell {TD} at (2,2) size 50x20 [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 32x16
                text run at (2,2) width 32: "50px"
            RenderTableCell {TD} at (54,2) size 20x20 [border: (1px inset #808080)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 16x16
                text run at (2,2) width 16: "1*"
            RenderTableCell {TD} at (76,2) size 50x20 [border: (1px inset #808080)] [r=0 c=2 rs=1 cs=1]
              RenderText {#text} at (2,2) size 32x16
                text run at (2,2) width 32: "50px"
            RenderTableCell {TD} at (128,2) size 20x20 [border: (1px inset #808080)] [r=0 c=3 rs=1 cs=1]
              RenderText {#text} at (2,2) size 16x16
                text run at (2,2) width 16: "3*"
      RenderBlock {P} at (0,74) size 784x16
        RenderText {#text} at (0,0) size 45x16
          text run at (0,0) width 45: "correct"
      RenderTable {TABLE} at (0,106) size 500x42 [border: (1px outset #808080)]
        RenderBlock {CAPTION} at (0,0) size 500x16
          RenderText {#text} at (121,0) size 258x16
            text run at (121,0) width 258: "Table with fixed width attribute =500px"
        RenderTableCol {COLGROUP} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
        RenderTableSection {TBODY} at (1,17) size 498x24
          RenderTableRow {TR} at (0,2) size 498x20
            RenderTableCell {TD} at (2,2) size 50x20 [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 32x16
                text run at (2,2) width 32: "50px"
            RenderTableCell {TD} at (54,2) size 194x20 [border: (1px inset #808080)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 16x16
                text run at (2,2) width 16: "1*"
            RenderTableCell {TD} at (250,2) size 50x20 [border: (1px inset #808080)] [r=0 c=2 rs=1 cs=1]
              RenderText {#text} at (2,2) size 32x16
                text run at (2,2) width 32: "50px"
            RenderTableCell {TD} at (302,2) size 194x20 [border: (1px inset #808080)] [r=0 c=3 rs=1 cs=1]
              RenderText {#text} at (2,2) size 16x16
                text run at (2,2) width 16: "3*"
      RenderBlock {P} at (0,164) size 784x48
        RenderText {#text} at (0,0) size 771x48
          text run at (0,0) width 523: "incorrect: it seems that the * columns start with the same width than the previous "
          text run at (522,0) width 249: "example, and then all the columnss are"
          text run at (0,16) width 331: "expanded proportionally to occupy the extra space. "
          text run at (330,16) width 438: "The * columns should use all the space unused by other columns, so"
          text run at (0,32) width 144: "no expand are needed "
          text run at (143,32) width 118: "for other columns."
      RenderTable {TABLE} at (0,228) size 784x42 [border: (1px outset #808080)]
        RenderBlock {CAPTION} at (0,0) size 784x16
          RenderText {#text} at (283,0) size 218x16
            text run at (283,0) width 218: "Table with width attribute =100%"
        RenderTableCol {COLGROUP} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
        RenderTableSection {TBODY} at (1,17) size 782x24
          RenderTableRow {TR} at (0,2) size 782x20
            RenderTableCell {TD} at (2,2) size 50x20 [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 32x16
                text run at (2,2) width 32: "50px"
            RenderTableCell {TD} at (54,2) size 336x20 [border: (1px inset #808080)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 16x16
                text run at (2,2) width 16: "1*"
            RenderTableCell {TD} at (392,2) size 50x20 [border: (1px inset #808080)] [r=0 c=2 rs=1 cs=1]
              RenderText {#text} at (2,2) size 32x16
                text run at (2,2) width 32: "50px"
            RenderTableCell {TD} at (444,2) size 336x20 [border: (1px inset #808080)] [r=0 c=3 rs=1 cs=1]
              RenderText {#text} at (2,2) size 16x16
                text run at (2,2) width 16: "3*"
      RenderBlock {P} at (0,286) size 784x16
        RenderText {#text} at (0,0) size 162x16
          text run at (0,0) width 162: "incorrect: same as above."
      RenderTable {TABLE} at (0,318) size 500x42 [border: (1px outset #808080)]
        RenderBlock {CAPTION} at (0,0) size 500x16
          RenderText {#text} at (140,0) size 220x16
            text run at (140,0) width 220: "Table with width attribute =500px"
        RenderTableCol {COLGROUP} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
        RenderTableSection {TBODY} at (1,17) size 498x24
          RenderTableRow {TR} at (0,2) size 498x20
            RenderTableCell {TD} at (2,2) size 122x20 [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 16x16
                text run at (2,2) width 16: "1*"
            RenderTableCell {TD} at (126,2) size 122x20 [border: (1px inset #808080)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 16x16
                text run at (2,2) width 16: "2*"
            RenderTableCell {TD} at (250,2) size 122x20 [border: (1px inset #808080)] [r=0 c=2 rs=1 cs=1]
              RenderText {#text} at (2,2) size 16x16
                text run at (2,2) width 16: "1*"
            RenderTableCell {TD} at (374,2) size 122x20 [border: (1px inset #808080)] [r=0 c=3 rs=1 cs=1]
              RenderText {#text} at (2,2) size 16x16
                text run at (2,2) width 16: "6*"
      RenderBlock {P} at (0,376) size 784x16
        RenderText {#text} at (0,0) size 45x16
          text run at (0,0) width 45: "correct"
      RenderTable {TABLE} at (0,408) size 784x42 [border: (1px outset #808080)]
        RenderBlock {CAPTION} at (0,0) size 784x16
          RenderText {#text} at (283,0) size 218x16
            text run at (283,0) width 218: "Table with width attribute =100%"
        RenderTableCol {COLGROUP} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
          RenderTableCol {COL} at (0,0) size 0x0
        RenderTableSection {TBODY} at (1,17) size 782x24
          RenderTableRow {TR} at (0,2) size 782x20
            RenderTableCell {TD} at (2,2) size 193x20 [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 16x16
                text run at (2,2) width 16: "1*"
            RenderTableCell {TD} at (197,2) size 193x20 [border: (1px inset #808080)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 16x16
                text run at (2,2) width 16: "2*"
            RenderTableCell {TD} at (392,2) size 193x20 [border: (1px inset #808080)] [r=0 c=2 rs=1 cs=1]
              RenderText {#text} at (2,2) size 16x16
                text run at (2,2) width 16: "1*"
            RenderTableCell {TD} at (587,2) size 193x20 [border: (1px inset #808080)] [r=0 c=3 rs=1 cs=1]
              RenderText {#text} at (2,2) size 16x16
                text run at (2,2) width 16: "6*"
      RenderBlock {P} at (0,466) size 784x16
        RenderText {#text} at (0,0) size 45x16
          text run at (0,0) width 45: "correct"
