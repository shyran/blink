layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock (anonymous) at (0,0) size 784x16
        RenderText {#text} at (0,0) size 270x16
          text run at (0,0) width 270: "This table should have cell spacing of 2px"
      RenderTable {TABLE} at (0,16) size 98x52 [border: (1px outset #808080)]
        RenderTableSection {TBODY} at (1,1) size 96x50
          RenderTableRow {TR} at (0,2) size 96x24
            RenderTableCell {TD} at (2,2) size 43x24 [border: (3px inset #000000)] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (4,4) size 35x16
                text run at (4,4) width 35: "AAA"
            RenderTableCell {TD} at (47,4) size 47x20 [border: (1px inset #808080)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 43x16
                text run at (2,2) width 43: "BBBB"
          RenderTableRow {TR} at (0,28) size 96x20
            RenderTableCell {TD} at (2,28) size 43x20 [border: (1px inset #808080)] [r=1 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 35x16
                text run at (2,2) width 35: "AAA"
            RenderTableCell {TD} at (47,28) size 47x20 [border: (1px inset #808080)] [r=1 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 43x16
                text run at (2,2) width 43: "BBBB"
      RenderBlock (anonymous) at (0,68) size 784x32
        RenderBR {BR} at (0,0) size 0x16
        RenderText {#text} at (0,16) size 270x16
          text run at (0,16) width 270: "This table should have cell spacing of 5px"
      RenderTable {TABLE} at (0,100) size 103x57 [border: (1px outset #808080)]
        RenderTableSection {TBODY} at (1,1) size 101x55
          RenderTableRow {TR} at (0,5) size 101x20
            RenderTableCell {TD} at (5,5) size 39x20 [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 35x16
                text run at (2,2) width 35: "AAA"
            RenderTableCell {TD} at (49,5) size 47x20 [border: (1px inset #808080)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 43x16
                text run at (2,2) width 43: "BBBB"
          RenderTableRow {TR} at (0,30) size 101x20
            RenderTableCell {TD} at (5,30) size 39x20 [border: (1px inset #808080)] [r=1 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 35x16
                text run at (2,2) width 35: "AAA"
            RenderTableCell {TD} at (49,30) size 47x20 [border: (1px inset #808080)] [r=1 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 43x16
                text run at (2,2) width 43: "BBBB"
      RenderBlock (anonymous) at (0,157) size 784x32
        RenderBR {BR} at (0,0) size 0x16
        RenderText {#text} at (0,16) size 404x16
          text run at (0,16) width 404: "This table should have cell spacingX of 5px, spacingY of 10px"
      RenderTable {TABLE} at (0,189) size 103x72 [border: (1px outset #808080)]
        RenderTableSection {TBODY} at (1,1) size 101x70
          RenderTableRow {TR} at (0,10) size 101x20
            RenderTableCell {TD} at (5,10) size 39x20 [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 35x16
                text run at (2,2) width 35: "AAA"
            RenderTableCell {TD} at (49,10) size 47x20 [border: (1px inset #808080)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 43x16
                text run at (2,2) width 43: "BBBB"
          RenderTableRow {TR} at (0,40) size 101x20
            RenderTableCell {TD} at (5,40) size 39x20 [border: (1px inset #808080)] [r=1 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 35x16
                text run at (2,2) width 35: "AAA"
            RenderTableCell {TD} at (49,40) size 47x20 [border: (1px inset #808080)] [r=1 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 43x16
                text run at (2,2) width 43: "BBBB"
      RenderBlock (anonymous) at (0,261) size 784x32
        RenderBR {BR} at (0,0) size 0x16
        RenderText {#text} at (0,16) size 404x16
          text run at (0,16) width 404: "This table should have cell spacingX of 5px, spacingY of 10px"
      RenderTable {TABLE} at (0,293) size 103x72 [border: (1px outset #808080)]
        RenderTableSection {TBODY} at (1,1) size 101x70
          RenderTableRow {TR} at (0,10) size 101x20
            RenderTableCell {TD} at (5,10) size 39x20 [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 35x16
                text run at (2,2) width 35: "AAA"
            RenderTableCell {TD} at (49,10) size 47x20 [border: (1px inset #808080)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 43x16
                text run at (2,2) width 43: "BBBB"
          RenderTableRow {TR} at (0,40) size 101x20
            RenderTableCell {TD} at (5,40) size 39x20 [border: (1px inset #808080)] [r=1 c=0 rs=1 cs=1]
              RenderText {#text} at (2,2) size 35x16
                text run at (2,2) width 35: "AAA"
            RenderTableCell {TD} at (49,40) size 47x20 [border: (1px inset #808080)] [r=1 c=1 rs=1 cs=1]
              RenderText {#text} at (2,2) size 43x16
                text run at (2,2) width 43: "BBBB"
