layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x64
  RenderBlock {html} at (0,0) size 800x64
    RenderBody {body} at (8,8) size 784x48
      RenderTable {table} at (0,0) size 200x48 [border: (1px outset #808080)]
        RenderTableSection (anonymous) at (1,1) size 198x46
          RenderTableRow {tr} at (0,2) size 198x20
            RenderTableCell {th} at (2,2) size 194x20 [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=2]
              RenderText {#text} at (2,2) size 190x16
                text run at (2,2) width 190: "This cell spans two columns"
          RenderTableRow {tr} at (0,24) size 198x20
            RenderTableCell {th} at (2,24) size 96x20 [border: (1px inset #808080)] [r=1 c=0 rs=1 cs=1]
              RenderText {#text} at (22,2) size 52x16
                text run at (22,2) width 52: "xxx xxx"
            RenderTableCell {th} at (100,24) size 96x20 [border: (1px inset #808080)] [r=1 c=1 rs=1 cs=1]
              RenderText {#text} at (22,2) size 52x16
                text run at (22,2) width 52: "xxx xxx"
