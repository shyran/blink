layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderTable {TABLE} at (0,0) size 252x140 [border: (1px outset #808080)]
        RenderTableSection {TBODY} at (1,1) size 250x138
          RenderTableRow {TR} at (0,2) size 250x48
            RenderTableCell {TD} at (2,2) size 246x47.91 [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=2]
              RenderBlock {H2} at (2,2) size 242x24
                RenderText {#text} at (0,0) size 174x24
                  text run at (0,0) width 174: "Personal Friends"
          RenderTableRow {TR} at (0,52) size 250x84
            RenderTableCell {TD} at (2,52) size 187x84 [border: (1px inset #808080)] [r=1 c=0 rs=1 cs=1]
              RenderBlock (anonymous) at (2,2) size 183x0
                RenderInline {FONT} at (0,0) size 0x0
                RenderText {#text} at (0,0) size 0x0
              RenderBlock {P} at (2,2) size 183x16
                RenderText {#text} at (0,0) size 183x16
                  text run at (0,0) width 183: "Alisha - Password-Protected"
              RenderBlock {P} at (2,34) size 183x16
                RenderText {#text} at (0,0) size 47x16
                  text run at (0,0) width 47: "Arlesia"
              RenderBlock {P} at (2,66) size 183x16
                RenderText {#text} at (0,0) size 37x16
                  text run at (0,0) width 37: "Asale"
            RenderTableCell {TD} at (191,52) size 57x84 [border: (1px inset #808080)] [r=1 c=1 rs=1 cs=1]
              RenderBlock {P} at (2,2) size 53x16
                RenderText {#text} at (0,0) size 35x16
                  text run at (0,0) width 35: "Ernie"
              RenderBlock {P} at (2,34) size 53x16
                RenderText {#text} at (0,0) size 53x16
                  text run at (0,0) width 53: "Melanie"
              RenderBlock {P} at (2,66) size 53x16
                RenderText {#text} at (0,0) size 49x16
                  text run at (0,0) width 49: "Tamara"
