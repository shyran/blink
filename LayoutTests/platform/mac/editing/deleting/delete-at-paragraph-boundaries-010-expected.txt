EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {DIV} at (0,0) size 784x284 [border: (2px solid #0000FF)]
        RenderBlock {DIV} at (14,14) size 756x96
          RenderText {#text} at (0,0) size 68x24
            text run at (0,0) width 68: "Tests: "
          RenderBR {BR} at (0,0) size 0x0
          RenderText {#text} at (0,24) size 734x48
            text run at (0,24) width 734: "Deleting when a selection starts at the beginning of a text following a nested"
            text run at (0,48) width 684: "block, and extends out of the enclosing block into the following block. "
          RenderBR {BR} at (683,66) size 1x0
          RenderText {#text} at (0,72) size 549x24
            text run at (0,72) width 266: "FIXME: Currently broken! "
            text run at (265,72) width 284: "See rdar://problem/4099839/."
        RenderBlock {DIV} at (14,126) size 756x144
          RenderText {#text} at (0,0) size 190x24
            text run at (0,0) width 190: "Expected Results: "
          RenderBR {BR} at (189,18) size 1x0
          RenderText {#text} at (0,24) size 744x120
            text run at (0,24) width 719: "Should see a total of five lines in the red box, with the third line inside of a"
            text run at (0,48) width 670: "nested red box. First line should be \"one\". Next line should be \"two\". "
            text run at (669,48) width 48: "Next"
            text run at (0,72) width 730: "line should be \"cha cha cha\", inside of a nested red box. Next line should be"
            text run at (0,96) width 52: "\"ur\". "
            text run at (51,96) width 368: "The last line should be \"cha cha cha\". "
            text run at (418,96) width 326: "Insertion point should be blinking"
            text run at (0,120) width 336: "at the start of the fourth line (\"ur\")."
      RenderBlock {DIV} at (0,308) size 784x128
        RenderBlock {DIV} at (0,0) size 784x128 [border: (2px solid #FF0000)]
          RenderBlock (anonymous) at (2,2) size 780x24
            RenderText {#text} at (0,0) size 35x24
              text run at (0,0) width 35: "one"
          RenderBlock {DIV} at (2,26) size 780x24
            RenderText {#text} at (0,0) size 36x24
              text run at (0,0) width 36: "two"
          RenderBlock {DIV} at (2,50) size 780x28 [border: (2px solid #FF0000)]
            RenderBlock {DIV} at (2,2) size 776x24
              RenderText {#text} at (0,0) size 112x24
                text run at (0,0) width 112: "cha cha cha"
          RenderBlock {DIV} at (2,78) size 780x24
            RenderText {#text} at (0,0) size 20x24
              text run at (0,0) width 20: "ur"
          RenderBlock {DIV} at (2,102) size 780x24
            RenderText {#text} at (0,0) size 112x24
              text run at (0,0) width 112: "cha cha cha"
caret: position 0 of child 0 {#text} of child 4 {DIV} of child 1 {DIV} of child 3 {DIV} of body
