layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x579
      RenderBlock {P} at (0,0) size 784x32
        RenderText {#text} at (0,0) size 776x32
          text run at (0,0) width 653: "This tests that inserting a newline at the beginning of quoted content doesn't add an extra quoted line. "
          text run at (652,0) width 124: "The test has passed"
          text run at (0,16) width 422: "if there are no empty lines (quoted or unquoted) in the box below."
      RenderBlock {DIV} at (0,48) size 784x80
        RenderBlock (anonymous) at (0,0) size 784x16
          RenderText {#text} at (0,0) size 60x16
            text run at (0,0) width 60: "unquoted"
          RenderBR {BR} at (59,12) size 1x0
        RenderBlock {BLOCKQUOTE} at (0,16) size 784x16 [color=#0000FF] [border: none (2px solid #0000FF)]
          RenderBlock {DIV} at (12,0) size 772x16
            RenderText {#text} at (0,0) size 98x16
              text run at (0,0) width 98: "quote level one"
        RenderBlock {BLOCKQUOTE} at (0,32) size 784x16 [color=#0000FF] [border: none (2px solid #0000FF)]
          RenderBlock {DIV} at (12,0) size 772x16
            RenderText {#text} at (0,0) size 98x16
              text run at (0,0) width 98: "quote level one"
        RenderBlock (anonymous) at (0,48) size 784x16
          RenderText {#text} at (0,0) size 60x16
            text run at (0,0) width 60: "unquoted"
          RenderBR {BR} at (59,12) size 1x0
        RenderBlock {BLOCKQUOTE} at (0,64) size 784x16 [color=#0000FF] [border: none (2px solid #0000FF)]
          RenderBlock {DIV} at (12,0) size 772x16
            RenderBlock {BLOCKQUOTE} at (0,0) size 772x16 [border: none (2px solid #0000FF)]
              RenderBlock {DIV} at (12,0) size 760x16
                RenderText {#text} at (0,0) size 99x16
                  text run at (0,0) width 99: "quote level two"
      RenderBlock {PRE} at (0,141) size 784x0
caret: position 8 of child 6 {#text} of child 3 {DIV} of body
