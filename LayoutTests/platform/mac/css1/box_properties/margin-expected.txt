layer at (0,0) size 800x600 scrollWidth 802 scrollHeight 2533
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x2533 backgroundClip at (0,0) size 800x600 clip at (0,0) size 800x600 outlineClip at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x2533
    RenderBody {BODY} at (8,8) size 784x2517 [bgcolor=#CCCCCC]
      RenderBlock {P} at (0,0) size 784x16
        RenderText {#text} at (0,0) size 363x16
          text run at (0,0) width 363: "The style declarations which apply to the text below are:"
      RenderBlock {PRE} at (0,32) size 784x91
        RenderText {#text} at (0,0) size 352x91
          text run at (0,0) width 344: ".zero {background-color: silver; margin: 0;}"
          text run at (343,0) width 1: " "
          text run at (0,13) width 352: ".one {margin: 0.5in; background-color: aqua;}"
          text run at (351,13) width 1: " "
          text run at (0,26) width 344: ".two {margin: 25px; background-color: aqua;}"
          text run at (343,26) width 1: " "
          text run at (0,39) width 352: ".three {margin: 5em; background-color: aqua;}"
          text run at (351,39) width 1: " "
          text run at (0,52) width 344: ".four {margin: 25%; background-color: aqua;}"
          text run at (343,52) width 1: " "
          text run at (0,65) width 164: ".five {margin: 25px;}"
          text run at (163,65) width 1: " "
          text run at (0,78) width 352: ".six {margin: -10px; background-color: aqua;}"
          text run at (351,78) width 1: " "
      RenderBlock {HR} at (0,136) size 784x2 [border: (1px inset #EEEEEE)]
      RenderBlock {P} at (0,154) size 784x32
        RenderText {#text} at (0,0) size 753x32
          text run at (0,0) width 753: "NOTE: The following tests are separated by class-zero paragraphs, so as to prevent margin-collapsing as described in"
          text run at (0,16) width 253: "section 4.1.1 of the CSS1 specification."
      RenderBlock {P} at (0,202) size 784x16 [bgcolor=#C0C0C0]
        RenderText {#text} at (0,0) size 208x16
          text run at (0,0) width 208: "This element has a class of zero."
      RenderBlock {P} at (48,266) size 688x16 [bgcolor=#00FFFF]
        RenderText {#text} at (0,0) size 673x16
          text run at (0,0) width 673: "This sentence should have an overall margin of half an inch, which will require extra text in order to test."
      RenderBlock {P} at (0,330) size 784x16 [bgcolor=#C0C0C0]
        RenderText {#text} at (0,0) size 208x16
          text run at (0,0) width 208: "This element has a class of zero."
      RenderBlock {P} at (25,371) size 734x16 [bgcolor=#00FFFF]
        RenderText {#text} at (0,0) size 655x16
          text run at (0,0) width 655: "This sentence should have an overall margin of 25 pixels, which will require extra text in order to test."
      RenderBlock {P} at (0,412) size 784x16 [bgcolor=#C0C0C0]
        RenderText {#text} at (0,0) size 208x16
          text run at (0,0) width 208: "This element has a class of zero."
      RenderBlock {P} at (80,508) size 624x32 [bgcolor=#00FFFF]
        RenderText {#text} at (0,0) size 599x32
          text run at (0,0) width 599: "This sentence should have an overall margin of 5 em, which will require extra text in order to"
          text run at (0,16) width 27: "test."
      RenderBlock {P} at (0,620) size 784x16 [bgcolor=#C0C0C0]
        RenderText {#text} at (0,0) size 208x16
          text run at (0,0) width 208: "This element has a class of zero."
      RenderBlock {P} at (196,832) size 392x48 [bgcolor=#00FFFF]
        RenderText {#text} at (0,0) size 386x48
          text run at (0,0) width 383: "This sentence should have an overall margin of 25%, which"
          text run at (0,16) width 386: "is calculated with respect to the width of the parent element."
          text run at (0,32) width 272: "This will require extra text in order to test."
      RenderBlock {P} at (0,1076) size 784x16 [bgcolor=#C0C0C0]
        RenderText {#text} at (0,0) size 208x16
          text run at (0,0) width 208: "This element has a class of zero."
      RenderBlock {UL} at (25,1117) size 734x114 [bgcolor=#00FFFF]
        RenderListItem {LI} at (40,0) size 694x16
          RenderListMarker at (-16,0) size 6x16: bullet
          RenderText {#text} at (0,0) size 382x16
            text run at (0,0) width 382: "This list has a margin of 25px, and a light blue background."
        RenderListItem {LI} at (40,16) size 694x16
          RenderListMarker at (-16,0) size 6x16: bullet
          RenderText {#text} at (0,0) size 269x16
            text run at (0,0) width 269: "Therefore, it ought to have such a margin."
        RenderListItem {LI} at (65,57) size 644x16
          RenderListMarker at (-16,0) size 6x16: bullet
          RenderText {#text} at (0,0) size 556x16
            text run at (0,0) width 556: "This list item has a margin of 25px, which should cause it to be offset in some fashion."
        RenderListItem {LI} at (40,98) size 694x16
          RenderListMarker at (-16,0) size 6x16: bullet
          RenderText {#text} at (0,0) size 304x16
            text run at (0,0) width 304: "This list item has no special styles applied to it."
      RenderBlock {P} at (0,1256) size 784x16 [bgcolor=#C0C0C0]
        RenderText {#text} at (0,0) size 208x16
          text run at (0,0) width 208: "This element has a class of zero."
      RenderBlock {P} at (-10,1262) size 804x48 [bgcolor=#00FFFF]
        RenderText {#text} at (0,0) size 789x48
          text run at (0,0) width 772: "This paragraph has an overall margin of -10px, which should make it wider than usual as well as shift it upward and pull"
          text run at (0,16) width 376: "subsequent text up toward it, and a light blue background. "
          text run at (375,16) width 395: "In all other respects, however, the element should be normal. "
          text run at (769,16) width 20: "No"
          text run at (0,32) width 539: "styles have been applied to it besides the negative margin and the background color."
      RenderBlock {P} at (0,1300) size 784x16 [bgcolor=#C0C0C0]
        RenderText {#text} at (0,0) size 208x16
          text run at (0,0) width 208: "This element has a class of zero."
      RenderTable {TABLE} at (0,1316) size 784x1201 [border: (1px outset #808080)]
        RenderTableSection {TBODY} at (1,1) size 782x1199
          RenderTableRow {TR} at (0,0) size 782x24
            RenderTableCell {TD} at (0,0) size 782x24 [bgcolor=#C0C0C0] [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=2]
              RenderInline {STRONG} at (0,0) size 163x16
                RenderText {#text} at (4,4) size 163x16
                  text run at (4,4) width 163: "TABLE Testing Section"
          RenderTableRow {TR} at (0,24) size 782x1175
            RenderTableCell {TD} at (0,599) size 12x24 [bgcolor=#C0C0C0] [border: (1px inset #808080)] [r=1 c=0 rs=1 cs=1]
              RenderText {#text} at (4,4) size 4x16
                text run at (4,4) width 4: " "
            RenderTableCell {TD} at (12,24) size 770x1175 [border: (1px inset #808080)] [r=1 c=1 rs=1 cs=1]
              RenderBlock {P} at (4,4) size 762x32
                RenderText {#text} at (0,0) size 753x32
                  text run at (0,0) width 753: "NOTE: The following tests are separated by class-zero paragraphs, so as to prevent margin-collapsing as described in"
                  text run at (0,16) width 253: "section 4.1.1 of the CSS1 specification."
              RenderBlock {P} at (4,52) size 762x16 [bgcolor=#C0C0C0]
                RenderText {#text} at (0,0) size 208x16
                  text run at (0,0) width 208: "This element has a class of zero."
              RenderBlock {P} at (52,116) size 666x32 [bgcolor=#00FFFF]
                RenderText {#text} at (0,0) size 643x32
                  text run at (0,0) width 643: "This sentence should have an overall margin of half an inch, which will require extra text in order to"
                  text run at (0,16) width 27: "test."
              RenderBlock {P} at (4,196) size 762x16 [bgcolor=#C0C0C0]
                RenderText {#text} at (0,0) size 208x16
                  text run at (0,0) width 208: "This element has a class of zero."
              RenderBlock {P} at (29,237) size 712x16 [bgcolor=#00FFFF]
                RenderText {#text} at (0,0) size 655x16
                  text run at (0,0) width 655: "This sentence should have an overall margin of 25 pixels, which will require extra text in order to test."
              RenderBlock {P} at (4,278) size 762x16 [bgcolor=#C0C0C0]
                RenderText {#text} at (0,0) size 208x16
                  text run at (0,0) width 208: "This element has a class of zero."
              RenderBlock {P} at (84,374) size 602x32 [bgcolor=#00FFFF]
                RenderText {#text} at (0,0) size 599x32
                  text run at (0,0) width 599: "This sentence should have an overall margin of 5 em, which will require extra text in order to"
                  text run at (0,16) width 27: "test."
              RenderBlock {P} at (4,486) size 762x16 [bgcolor=#C0C0C0]
                RenderText {#text} at (0,0) size 208x16
                  text run at (0,0) width 208: "This element has a class of zero."
              RenderBlock {P} at (194.50,692.50) size 381x48 [bgcolor=#00FFFF]
                RenderText {#text} at (0,0) size 370x48
                  text run at (0,0) width 340: "This sentence should have an overall margin of 25%,"
                  text run at (0,16) width 370: "which is calculated with respect to the width of the parent"
                  text run at (0,32) width 59: "element. "
                  text run at (58,32) width 273: "This will require extra text in order to test."
              RenderBlock {P} at (4,931) size 762x16 [bgcolor=#C0C0C0]
                RenderText {#text} at (0,0) size 208x16
                  text run at (0,0) width 208: "This element has a class of zero."
              RenderBlock {UL} at (29,972) size 712x114 [bgcolor=#00FFFF]
                RenderListItem {LI} at (40,0) size 672x16
                  RenderListMarker at (-16,0) size 6x16: bullet
                  RenderText {#text} at (0,0) size 382x16
                    text run at (0,0) width 382: "This list has a margin of 25px, and a light blue background."
                RenderListItem {LI} at (40,16) size 672x16
                  RenderListMarker at (-16,0) size 6x16: bullet
                  RenderText {#text} at (0,0) size 269x16
                    text run at (0,0) width 269: "Therefore, it ought to have such a margin."
                RenderListItem {LI} at (65,57) size 622x16
                  RenderListMarker at (-16,0) size 6x16: bullet
                  RenderText {#text} at (0,0) size 556x16
                    text run at (0,0) width 556: "This list item has a margin of 25px, which should cause it to be offset in some fashion."
                RenderListItem {LI} at (40,98) size 672x16
                  RenderListMarker at (-16,0) size 6x16: bullet
                  RenderText {#text} at (0,0) size 304x16
                    text run at (0,0) width 304: "This list item has no special styles applied to it."
              RenderBlock {P} at (4,1111) size 762x16 [bgcolor=#C0C0C0]
                RenderText {#text} at (0,0) size 208x16
                  text run at (0,0) width 208: "This element has a class of zero."
              RenderBlock {P} at (-6,1117) size 782x48 [bgcolor=#00FFFF]
                RenderText {#text} at (0,0) size 772x48
                  text run at (0,0) width 772: "This paragraph has an overall margin of -10px, which should make it wider than usual as well as shift it upward and pull"
                  text run at (0,16) width 376: "subsequent text up toward it, and a light blue background. "
                  text run at (375,16) width 391: "In all other respects, however, the element should be normal."
                  text run at (0,32) width 563: "No styles have been applied to it besides the negative margin and the background color."
              RenderBlock {P} at (4,1155) size 762x16 [bgcolor=#C0C0C0]
                RenderText {#text} at (0,0) size 208x16
                  text run at (0,0) width 208: "This element has a class of zero."
