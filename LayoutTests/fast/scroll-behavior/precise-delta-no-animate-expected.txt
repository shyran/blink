Tests that a WebMouseWheelEvent with hasPreciseScrollingDeltas does not produce an animated scroll

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


PASS element.scrollTop is 80
PASS successfullyParsed is true

TEST COMPLETE

