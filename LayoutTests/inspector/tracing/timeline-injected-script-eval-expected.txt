Tests the Timeline API function call record for InjectedScript.eval call feature.

FunctionCall Properties:
{
    children : <object>
    data : {
        frame : <string>
        scriptId : <string>
        scriptLine : <number>
        scriptName : <string>
    }
    endTime : <number>
    frameId : <string>
    startTime : <number>
    thread : <string>
    type : "FunctionCall"
}

